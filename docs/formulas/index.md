# Formulas

Formulas provide a way for users to write expressions to perform specific queries.
It is a Domain Specific Language (DSL) that provides an easier to method to
query data and perform computations.

There are two types of formulas: entities and signals.

## DateTime and Durations

Formulas use [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) to express date
and time, and durations.

## Entities

Entity formulas

### asset()

### hardpoint()

### device()

### sensor()

## Signals

### constant(stride="PT1S",value=1)

Generate a constant signal for the given value.

### derivative()

### difference(a, b)

Substract signal *b* from signal *a*.

$$a - b$$

#### Examples

- Difference of TIP000012 pressure and TIP000011 pressure.

    ```text
    difference(
      sensor(device(sn='TIP000012'), name='pressure'),
      sensor(device(sn='TIP000011'), name='pressure')
    )
    ```

### multiply(signal,signal,…)

Multiply signals

### rollingaverage(signal,stride="PT5M",window="PT15M")

Calculate rolling average on the given signal, stride, and window.

#### Examples

- Calculate a rolling average on TIP000012 pressure on a 1 hour window every 10 minutes

    ```text
    rolling(
      sensor(device(sn='TIP000012'), name='pressure'),
      stride="PT10M",
      window="PT1H"
    )
    ```

### sine(period="PT1M",stride="PT1S")

Generate a sine wave with the given period

### vfdstatusfilter(signal,quiet="PT1M",stride="PT1S")

Convert a VFD status signal to a filter. The VFD status signal is 0 to indicate
the pump is off, and 1 when it is on. This function maps transitions locates
times when the signal changes from 0 to 1 or 1 to 0. Let's call these times
$t_A$ and $t_B$. Then for the given *quiet* period $q$, it will return

$$
f(t) =  \begin{cases}
  \text{null}, & \text{if } t \in [t_A - q, t_B + q) \\
  1, & \text{otherwise}
\end{cases}
$$

--8<-- "mathjax.txt"
