# Set login context tenant key

Set the current login context tenant key. This is used when switching tenants.

## Signature

```typescript
illumass.auth.setLoginContextTenantKey(tenantKey: string | undefined)
```

## Access control

Signed in users can call this.

## Arguments

* *tenantKey* is either a valid tenant key, or `undefined`.

## Resolves

Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[ActionContext](../interfaces/action-context.md)&gt;.

## Implementation notes

If *tenantKey* is a tenant key, then all subsequent requests should check `volatile.illumass.tenantKey`.
If it is falsy, set `volatile.illumass.tenantKey` to *tenantKey*.
