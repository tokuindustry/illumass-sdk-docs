# Export csv

Export a slice as a csv

## Signature

```typescript
illumass.signal.exportCsv(
  signal: SignalDescriptor,
  interval: IntervalDescriptor,
  localTimeZone?: moment.MomentZone)
)
```

## Access control

User needs to be granted view permission on the device or monitoring point of
the signal.

## Arguments

* *signal* is a [SignalDescriptor](../interfaces/signal-descriptor.md) that
describes the signal to fetch slice from.
* *interval* is a [IntervalDescriptor](../interfaces/interval-descriptor.md)
that describes the interval fetch slice from.
* *localTimeZone* can be optionally specified. It is the time zone to use for
the local time zone columns.

## Resolves

Resolves to a [AnnotatedData](../../common/interfaces/annotated-data.md)
&lt;string&gt;. The string is the CSV. It is up to the caller to then save
or download the string, depending on the environment.

The returned CSV has 5 columns.

| Column | Description |
|:---|:---|
| Date UTC | UTC Date |
| Time UTC | UTC Time |
| Date Local | Local Date. Include timezone in header. e.g. (ICT) |
| Time Local | Local Time. Include timezone in header. |
| Value | Signal value. Include units in header. |
