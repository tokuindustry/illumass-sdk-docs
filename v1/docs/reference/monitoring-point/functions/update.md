## Update
Update monitoring point

### Signature
```
illumass.monitoringPoint.update(change: MonitoringPointChange)
```

### Access control
User must be signed in and must have edit permission on the asset of the
monitoring point.

### Arguments

* _change_ is the [monitoring point](../../../entities/monitoring-points.md) to be updated. `key` must be specified and it should
    have the form `/monitoringPoints/<monitoring point id>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MonitoringPoint](../../../entities/monitoring-points.md)&gt;.
It contains the updated monitoring point.

Throws a 404 Not Found error if asset with _change.key_ does not exist.
