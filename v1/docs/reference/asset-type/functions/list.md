## List
List asset types that user has View permission for.

### Signature
```
illumass.assetType.list()
```

### Access control
User must be signed in to access.

### Arguments

There are no arguments.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[AssetType](../../../entities/asset-types.md)&gt;&gt;.
