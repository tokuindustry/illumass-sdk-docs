# Delete API key

Revoke an API key

## Examples

=== "Typescript"

    ```typescript
    const apiKey = await illumass.auth.deleteeApiKey(apiKeyId);
    ```

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl -H 'Authorization: Bearer <jwt>' \
    -X DELETE \
    'https://api.illumass.com/v4/api-keys/<apiKeyId>'
    ```

    **WebSocket**

    ```json
    {
      "controller": "auth",
      "action": "deleteApiKey",
      "_id": "api-key-id"
    }
    ```

    **Response**

    ```json
    {
      "requestId": "04f00fc5-cb16-404f-b20c-882b1c7cf097",
      "status": 200,
      "error": null,
      "controller": "auth",
      "action": "deleteApiKey",
      "collection": null,
      "index": null,
      "volatile": null,
      "result": {
        "_id": "oOJ9CHQB7KmymBNZ3uuL"
      }
    }
    ```

## Parameters

- *apiKeyId*
    API key id
