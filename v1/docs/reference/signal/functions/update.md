## Update
Update signal

### Signature
```
illumass.signal.update(change: SignalChange)
```

### Access control
User must be signed in and must have Edit permission on the holder.

### Arguments

* _change_ is the [signal](../../../entities/signals.md) to be updated. `key`
  must be specified and it should have the form `/signals/<signal id>`. Only
  _alarmConfig_ can be edited.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Signal](../../../entities/signals.md)&gt;.
It contains the updated signal.

Throws a 404 Not Found error if signal with _change.key_ does not exist.
