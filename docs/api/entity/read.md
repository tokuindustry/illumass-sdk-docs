# Read

Read an entity.

## Examples

=== "Typescript"

    ```typescript
    const country = await illumass.country.read('country.ca');
    ```

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl -H 'Authorization: Bearer <jwt>' \
    'https://api.illumass.com/v4/entity/country.ca'
    ```

    **WebSocket**

    ```json
    {
      "action": "read",
      "controller": "rumble/entity",
      "ref": "country.ca",
    }
    ```

    **Response**

    ```json
    {
      "entityType": "country",
      "id": "EQwyrNETqxn-u-XkD1KkUQ7WPYc",
      "modified": "2020-07-24T19:38:24.007Z",
      "modifier": {
        "entityType": "user",
        "id": "v76O3UhDxPcebRBmXU7mi_i-TME",
        "slug": "toku-systems.superuser",
        "shortName": "Superuser"
      },
      "slug": "ca",
      "name": "Canada",
      "code": "CA"
    }
    ```

## Parameters

- *ref*

    Required. The [entity reference](../../entities/references) to read *e.g. country.ca*.
