## Archive
Archive a comment

### Signature
```
illumass.comment.archive(commentKey: string)
```

### Access control
User must be signed in and must have edit permission on the linked entity.

### Arguments

* _commentKey_ is the comment to be archived. It should have the
    form `/comments/<comment id>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Comment](../../../entities/comments.md)&gt;.
It contains the comment before it was archived.

Throws a 404 Not Found error if comment with _commentKey_ does not exist.
