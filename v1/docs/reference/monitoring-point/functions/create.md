## Create
Create monitoring point

### Signature
```
illumass.monitoringPoint.create(change: MonitoringPointChange)
```

### Access control
User must be signed in can edit the asset _monitoringPoint.assetKey_.

### Arguments

* _change_ is the [monitoring point](../../../entities/monitoring-points.md) to create.
  If `key` is not specified, it will be auto-generated. If it is specified, it
  should have the form `/monitoringPoints/<client id>`. The client id should be
  a value that has a very high probability of being unique. For example
    [uuid](https://www.npmjs.com/package/uuid) v4 should be adequate, as well as
    more compact forms of ids.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MonitoringPoint](../../../entities/monitoring-points.md)&gt;.
