# Devices
Devices have one or more sensors. It can be thought of as the piece of equipment
responsible for communicating with Illumass.

```
{
  "lastAudit": {
    "userKey": "",
    "timestamp": "2019-05-15T20:23:49.287Z"
  },
  "key": "/devices/TIP000123",
  "archived": false,
  "serialNumber": "TIP000123",
  "tenantKey": "/tenants/AzQP0RteZsfOflRC",
  "geometry": null,
  "status": {
    "alarm": "high"
  }
}
```

## Properties

* _serialNumber_ device serial number.
* _tenantKey_ is the tenant the device belongs to.
* _geometry_ describes the geo location of the device.
  This is typically a geojson point. Future models will have GPS feature and
  will report its location back to Illumass.
* _status_ is the status of the device
