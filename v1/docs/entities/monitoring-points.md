# Monitoring Points

Monitoring points (aka Measurements) represent some aspect of an asset that is
being monitored or measured.

Note that there are different types of monitoring points.

## Install points
An install point is a monitoring point where hardware installed to directly
observe a physical process. This is where are T1000 devices will be installed,
as well as other types of devices, like SCADA flow meters. An install point is
represented as follows

```
{
  "status": {
    "alarm": "nominal"
  },
  "primarySignalTypeKey": "/signalTypes/processStaticPressure",
  "archived": false,
  "inputMonitoringPoints": {},
  "name": "Casing",
  "deviceKey": "/devices/TIP001094",
  "type": "installPoint",
  "key": "/monitoringPoints/-LTFHYYxhvMK2bHkJq-Y",
  "assetKey": "/assets/-LTFHUFsaY6qF-Mqfkjh",
  "lastAudit": {
    "userKey": ""
  },
  "props": {}
}
```

Note that in this case _type_ is `installPoint` and _deviceKey_ is the device
that is installed.

### Install schedule
One important concept to understand for install points is the
*install schedule*. Since over time different devices can be installed at the
same monitoring point, a schedule is used to track when a specific device was
installed. For example, the a single install point you could have an install
schedule of

| Start | End | Monitoring Point | Device |
|:---|:---|:---|:---|
| 2019-01-01T06:00:00Z | 2019-02-01T06:00:00Z | /monitoringPoints/-LTFHYYxhvMK2bHkJq-Y | /devices/TIP001094 |
| 2019-02-01T06:00:00Z | 2019-03-01T06:00:00Z | /monitoringPoints/-LTFHYYxhvMK2bHkJq-Y | /devices/TIP001011 |
| 2019-03-01T06:00:00Z | 2019-04-01T06:00:00Z | /monitoringPoints/-LTFHYYxhvMK2bHkJq-Y | None |
| 2019-04-01T06:00:00Z | N/A | /monitoringPoints/-LTFHYYxhvMK2bHkJq-Y | /devices/TIP002031 |

Timestamps are in ISO-8601 format. The above table shows the install schedule for
*/monitoringPoints/-LTFHYYxhvMK2bHkJq-Y*. There are 4 intervals represented:

* From January 1, 2019 at 6:00 AM UTC to February 1, 2019 at 6:00 AM UTC (exclusive)
  device */devices/TIP001094* was installed.
* From February 1, 2019 at 6:00 AM UTC to March 1, 2019 at 6:00 AM UTC (exclusive)
  device */devices/TIP001011* was installed.
* From March 1, 2019 at 6:00 AM UTC to April 1, 2019 at 6:00 AM UTC (exclusive)
  no device was installed.
* As of April 1, 2019 at 6:00 AM UTC device */devices/TIP002031* is installed.

Note that there is also an install schedule for a device. It is the monitoring
points that a device has been installed on.

## Differential
A differential calculates and measures the difference between upstream and
downstream monitoring points.

```
{
  "lastAudit": {
    "userKey": ""
  },
  "key": "/monitoringPoints/-LSR9A7b-ziZ_d2KBkG0",
  "archived": false,
  "name": "Differential: License# 28802, Line# 1",
  "assetKey": "/assets/-LSR98Zz9JMpee42LvOK",
  "type": "differential",
  "inputMonitoringPoints": {
    "upstream": "/monitoringPoints/-LP6VAJlY8O1baCHQacu",
    "downstream": "/monitoringPoints/-LP6qsOWXAscgg3Z-Xz1"
  },
  "props": {},
  "primarySignalTypeKey": "/signalTypes/processStaticPressure",
  "deviceKey": null,
  "status": {
    "alarm": "nominal"
  }
}
```

Note that the _type_ is `differential`. Furthermore _inputMonitoringPoints_
has an _upstream_ and _downstream_ monitoring point keys.

## Multiply and delay
A multiply and delay monitoring point will take as input a source monitoring
point and apply a multiplier and delay to it.

This is typically used as an input for a differential. Ideally, the downstream
signal is a scaled down, delayed form of the upstream signal. If you choose the
appropriate multiplier and delay and apply it to the upstream signal, it will
closely match the downstream signal, resulting in a flat differential signal of 0.

```
{
  "lastAudit": {
    "userKey": ""
  },
  "key": "/monitoringPoints/000b0000000000fb",
  "archived": false,
  "name": "Modified",
  "assetKey": "/assets/000a000000000068",
  "type": "multiplyAndDelay",
  "inputMonitoringPoints": {
    "source": "/monitoringPoints/000b000000000027"
  },
  "props": {
    "multiplierAndDelay": {
      "delay": 50,
      "multiplier": 0.82
    }
  },
  "primarySignalTypeKey": "/signalTypes/processStaticPressure",
  "deviceKey": null,
  "status": {
    "alarm": "nominal"
  }
}
```

Note that the _type_ is `multiplyAndDelay`. _inputMonitoringPoints_ has a
_source_ monitoring point key. _props_ contains a _multiplierAndDelay_ object
that specifies the _multiplier_ and _delay_ (in seconds) to be applied to the
source primary signal type.

## Properties

* _primarySignalTypeKey_ is the primary signal type of the device. For devices
  like T1000 there is usually one signal that is considered to be the primary
  one. When showing summary data, it is a hint to the UI as to which signal to
  use.
* _name_ of the monitoring point. Usually this is the name of a specific part or
  physical process being measured.
* _inputMonitoringPoints_ specifies the inputs for calculated monitoring points.
* _deviceKey_ for install points, this is the device installed on the monitoring
  point.
* _type_ of the monitoring point
  Currently there are 3 types.

    * _installPoint_ for direct physical observations.
    * _differential_ for difference in pressure between upstream and
      downstream monitoring points
    * _multiplyAndDelay_ to apply a multiplier and delay to a source monitoring point.

* _assetKey_ is the asset the monitoring point is measuring some aspect of.
* _geometry_ is the location and shape of the monitoring point. It follows [GeoJSON](https://geojson.org/) format.
