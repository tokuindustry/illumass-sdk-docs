## List
List signal types

### Signature
```
illumass.signalType.list()
```

### Access control
User must be signed in to access.

### Arguments

There are no arguments.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[SignalType](../../../entities/signal-types.md)&gt;&gt;.
