## Get root
Get monitoring point by device key. This is the device installed on the monitoring point.

### Signature
```
illumass.monitoringPoint.getByDeviceKey(deviceKey: string)
```

### Access control
User must be signed in to access.

### Arguments

* _deviceKey_ of device.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MonitoriingPoint](../../../entities/monitoring-points.md)&gt;.

If no such monitoring point exists, 404 Not Found should be thrown.
