## Subscribe
Subscribe to a single signal by key

### Signature
```
illumass.signal.subscribe(signalKey: string, callback:(change: AnnotatedData<Signal>) => void)
```

### Access control
User must be signed in to access and have View permission on the holder.

### Arguments

* _signalKey_ of signal.
* _callback_ if function that is called every time signal is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Signal](../../../entities/signals.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).

Throws a 404 Not Found error if signal with _signalKey_ does not exist.
