## List comments by link key
List all comments attached to an entity.

### Signature
```
illumass.comment.listByLinkKey(linkKey: string)
```

### Access control
User must be signed in to access.

### Arguments

* _linkKey_ of entity to list comments for.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Comment](../../../entities/comments.md)&gt;&gt;.
