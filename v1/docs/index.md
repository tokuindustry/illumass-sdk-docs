# Guides

## Development Tools

The Kuzzle admin tool can be used to browse and search data stored in Kuzzle.
See [here](guides/getting-started/kuzzle-admin.md) for more information.

## Concepts

* [Login context](guides/concepts/login-context.md)
* [Security roles](guides/concepts/security-roles.md)

## Fixtures

### Examples

* [Signals](guides/fixtures/examples/signals.md)
