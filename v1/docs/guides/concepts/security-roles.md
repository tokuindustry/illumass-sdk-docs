# Security Roles

A security role determines what level of access a user has. There
are two kinds of security roles: [user roles](#user-roles) and [asset roles](#asset-roles).

## User roles

A user has exactly one user role. A user is assigned a role by setting the
[_userType_](../../entities/users.md) field.

### Customer Member (*userType = 'member'*)

Users can sign into Customer account. However, they cannot see very much until
they are assigned an asset role.
  
### Customer Administrator (*userType = 'administrator'*)

Users can sign into Customer account and can see and change anything.

### Auditor (*userType = 'auditor'*)

Users can sign into any Customer account (via Switch Customer feature), and can
view anything but not change anything.
  
Typically reserved for internal staff or researchers. It is only available to
the TOKU Industry customer.

### Customer Support (*userType = 'customerSupport'*)

Users can sign into any Customer account (via Switch Customer feature), and can
view everything AND change everything.
  
Typically reserved for internal staff. It is only available to the TOKU
Industry customer.

### Superuser (*userType = 'superuser'*)

They can sign into any Customer account (via Switch Customer feature), can view
everything and change everything. They also have access to special maintenance functions.
  
It is not available as an option, and requires a special procedure to make grant
a user the Superuser role.

## Asset roles

A user can optionally have 1 or more asset roles. Note that in practice most
users will have at least one asset role.

A user is assigned to an asset role by setting the asset's [_roleMembers_](../../entities/assets.md)
field.

For the purposes of this discussion, an **asset's subtree** refers to that asset
and all assets under it.

The system automicatically creates 3 asset roles for each customer.

### Viewers (*roleMembers.viewers*)

Users assigned as an asset's viewer will be able to

* view asset's subtree

They do not receive alarm notifications.

### Operators (*roleMembers.operators*)

Users assigned as an asset's operator will be able to

* view asset's subtree
* clear alarms in asset's subtree

They will also receive notifications for alarms that originate from the asset's subtree.

### Managers (*roleMembers.managers*)

Users assigned as an asset's manager will be able to

* view and edit asset's subtree
* make changes to alarm settings in the asset's subtree
* clear alarms in asset's subtree

They will also receive notifications for alarms that originate from the asset's subtree.
