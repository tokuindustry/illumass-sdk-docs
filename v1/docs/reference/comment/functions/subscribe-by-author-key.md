## Subscribe by author key
Subscribe to comments from a given author.

### Signature
```
illumass.comment.subscribeByAuthorKey(authorKey: string, callback:(change: AnnotatedData<Comment>) => void)
```

### Access control
User must be signed in to access and have View permission on the linked entity.

### Arguments

* _authorKey_ of author.
* _callback_ if function that is called every time comment is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Comment](../../../entities/comments.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
