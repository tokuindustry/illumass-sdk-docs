## List by holder key
List signals by its holder key.

### Signature
```
illumass.signal.listByHolderKey(holderKey: string)
```

### Access control
User must be signed in to access and have View permission on the holder.

### Arguments

* _holderKey_ is the device on monitoring point that the signal belongs to.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Signal](../../../entities/signals.md)&gt;&gt;.
