## Get
Get measurement unit

### Signature
```
illumass.measurementUnit.get(measurementUnitKey: string)
```

### Access control
User must be signed in to access.

### Arguments

* _measurementUnitKey_ of measurement unit.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MeasurementUnit](../../../entities/measurement-units.md)&gt;.
