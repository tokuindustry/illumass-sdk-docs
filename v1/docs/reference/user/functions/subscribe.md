## Subscribe
Subscribe to a single user by key

### Signature
```
illumass.user.subscribe(userKey: string, callback:(change: AnnotatedData<User>) => void)
```

### Access control
User must be signed in to access and have View permission on _userKey_.

### Arguments

* _userKey_ of user.
* _callback_ if function that is called every time user is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[User](../../../entities/users.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
