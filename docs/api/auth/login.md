# Login

Login to Illumass with email and password. On a successful login, a JWT is
returned that should be included with all subsequent requests.

## Examples

=== "Typescript"

    ```typescript
    const jwt = await illumass.auth.login(email, password, expiresIn);
    ```

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl --data '{ "email": "<email>", "password": "<password>" }' \
    -H 'Content-Type: application/json' \
    https://api.illumass.com/v4/login?expiresIn=1h
    ```

    **WebSocket**

    ```json
    {
      "controller": "auth",
      "action": "login",
      "strategy": "illumass",
      "body": {
        "email": "<email>",
        "password": "<password>"
      },
      "expiresIn": "1h"
    }
    ```

    **Response**

    ```json
    {
      "requestId": "8d8671a1-2db6-43f9-ab00-73a67d6e228f",
      "status": 200,
      "error": null,
      "controller": "auth",
      "action": "login",
      "collection": null,
      "index": null,
      "volatile": null,
      "result": {
        "_id": "0005000000000008",
        "expiresAt": 1597687105283,
        "jwt": "<jwt>",
        "ttl": 3600000
      }
    }
    ```

## Parameters

- *email*

    Required. User's email.

- *password*

    Required. User's password.

- *expiresIn*

    Optional. Defaults to 1 hour. Time-to-live for JWT, expressed as a format that
    can be parsed by the [ms library](https://www.npmjs.com/package/ms).
