## Subscribe all
Subscribe to all tenants that user can view.

### Signature
```
illumass.tenant.subscribeAll(callback:(change: AnnotatedData<Tenant>) => void)
```

### Access control
User must be signed in to access.

### Arguments

* _callback_ if function that is called every time a tenant is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Tenant](../../../entities/tenants.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
