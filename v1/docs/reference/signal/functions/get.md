## Get
Get signal

### Signature
```
illumass.signal.get(signalKey: string)
```

### Access control
User must be signed in to access.

### Arguments

* _signalKey_ of signal

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Signal](../../../entities/signals.md)&gt;.
