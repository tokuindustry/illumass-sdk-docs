# Signals

This yaml file shows a tenant with a device that has a process pressure and
temperature signal.

Note that the monitoring point that the device is installed on also has the
same type of signals created.

```yaml
tenants:
  ${tenantId:16}:
    name: ${tenantName}

users:
  ${user1Id:16}:
    firstName: ${firstName1}
    lastName: ${lastName1}
    email: ${user1Id}@test.com
    password: ${password1}
    userType: administrator

  ${user2Id:16}:
    firstName: ${firstName2}
    lastName: ${lastName2}
    email: ${user2Id}@test.com
    password: ${password2}

devices:
  # Create a device with a generated serial number
  TEST-${serialNumber:6:upper}:
    serialNumber: TEST-${serialNumber}

assets:
  ${asset1Id:16}:
    name: Test Asset 1
    parentKey: "assets:"
    assetTypeKey: assetTypes:well
    roleMembers:
      operators:
        0: /users/${user2Id}

monitoringPoints:
  # Create a monitoring point for our asset.
  # The device specified at 'deviceKey' indicates what device is installed.
  ${monitoringPoint1Id:16}:
    name: Tubing
    assetKey: /assets/${asset1Id}
    deviceKey: /devices/TEST-${serialNumber}
    type: installPoint

signals:
  # Device pressure and temperature signals
  ${devicePressureSignalId}:
    holderKey: /devices/TEST-${serialNumber}
    signalTypeKey: /signalTypes/processStaticPressure
  ${deviceTemperatureSignalId}:
    holderKey: /devices/TEST-${serialNumber}
    signalTypeKey: /signalTypes/staticPressureSensorTemperature

  # Monitoring point pressure and temperature signals
  ${mpPressureSignalId}:
    holderKey: /monitoringPoints/${monitoringPoint1Id}
    signalTypeKey: /signalTypes/processStaticPressure
  ${mpTemperatureSignalId}:
    holderKey: /monitoringPoints/${monitoringPoint1Id}
    signalTypeKey: /signalTypes/staticPressureSensorTemperature
```
