# Alarm
An alarm that is [triggered on a signal](./signals.md#triggering-an-alarm).

```
{
  "lastAudit": {
    "userKey": "",
    "timestamp": "2019-05-18T02:19:13.033Z"
  },
  "key": "/alarms/0018000000000001",
  "archived": false,
  "signalKey": "/signals/000300000000001d",
  "alarmType": "high",
  "started": "2019-05-18T02:19:09.460Z",
  "created": "2019-05-18T02:19:12.903Z",
  "updated": "2019-05-18T02:19:12.903Z",
  "threshold": 5400,
  "reading": 5467.46,
  "cleared": null,
  "clearerKey": null
}
```

## Properties
* _signalKey_ of the signal that the alarm is triggered on.
* _alarmType_ is one of *lowLow*, *low*, *high*, or *highHigh*.
* _started_ is an ISO-8601 timestamp of when the alarm started.
* _threshold_ is the *limit* value at the time the alarm was triggered.
* _reading_ is the first value that crossed the *limit*.
* _cleared_ is an ISO-8601 timestamp of when the alarm was cleared. It is *null*
  if it hasn't been cleared.
* _clearerKey_ is the key of the user who cleared the alarm. It is *null* if it
  hasn't been cleared.
