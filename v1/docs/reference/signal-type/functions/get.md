## Get
Get signal type

### Signature
```
illumass.signalType.get(signalTypeKey: string)
```

### Access control
User must be signed in to access.

### Arguments

* _signalTypeKey_ of signal type

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[SignalType](../../../entities/signal-types.md)&gt;.
