## Update status

Allows developers and testers to manually change the status of a signal,
device, monitoring point, or asset.

### Signature

`illumass.testing.updateStatus(key: string, alarmStatus: AlarmEnum, timestamp: moment.Moment, reading: number)`

### Access control
User needs to be granted system admin permission.

### Arguments
* _key_ of a signal, device, monitoring point, or asset.
* new _alarmStatus_. Note that _AlarmEnum_ is exported in `illumass-entity.ts`.
* new _timestamp_
* new _reading_

### Resolves
Resolves if call succeeded.
