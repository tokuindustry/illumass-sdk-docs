## List by signal type key
List signals by signal type key.

### Signature
```
illumass.signal.listBySignalTypeKey(signalTypeKey: string)
```

### Access control
User must be signed in to access and have View permission on the holder.

### Arguments

* _signalTypeKey_ is the signal type to get signals for

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Signal](../../../entities/signals.md)&gt;&gt;.
