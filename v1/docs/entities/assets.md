# Assets

Assets (aka Locations) represent customer assets that are being monitored
and measured. An asset may have 0 or more monitoring point, with each monitoring
point measuring some aspect of the asset.

Note an asset can be a geographic area like an oil field (e.g. permian basin),
a piece of equipment (e.g. pumpjack), or a specific part of a facility. This
will large depend on how our users choose to model their system as assets in
Illumass.

Also note that Assets behave like folders in a file system. Every asset can
have child assets.

```
{
  "lastAudit": {
    "userKey": "",
    "timestamp": "2019-05-16T20:06:51.686Z"
  },
  "key": "/assets/HtSkPMyvi3SPytHE",
  "archived": false,
  "assetTypeKey": "/assetTypes/8009000000000004",
  "name": "Hanoi",
  "parentKey": "/assets/0012000000000451",
  "roleMembers": {
    "managers": {
      "/users/q1LFYvisBRdVfvrI": {
        shortName: "Kev0Pm5z LbNu8MaJ"
      }
    }
  },
  "props": {},
  "geometry": {
    "type": "Point",
    "coordinates": [
      105.854167,
      21.028333
    ],
    "crs": {
      "type": "name",
      "properties": {
        "name": "EPSG:4326"
      }
    }
  }
}
```

## Properties

* _assetTypeKey_ is a key to [asset types](./asset-types.md) that represents the type of the asset.
* _name_ of the asset
* _parentKey_ is the key of the parent this asset is a child of. This forms a
  hierarchy that is used for security and notifications.
* _roleMembers_ indicates which users belong to a certain role for the asset.
  Currently there are [3 roles](./asset-roles.md).
* _geometry_ is the location and shape of the asset. It follows [GeoJSON](https://geojson.org/) format.
* _status_ is the status of the asset
