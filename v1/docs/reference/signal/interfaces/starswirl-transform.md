# Transform

Describes a transform to apply

```typescript
interface ITransform {

    /** Transform name */
    name?: (string|null);

    /** Transform identity */
    identity?: (toku.starswirl.transform.protobuf.IIdentity|null);

    /** Transform movingAverage */
    movingAverage?: (toku.starswirl.transform.protobuf.IMovingAverage|null);

    /** Transform derivative */
    derivative?: (toku.starswirl.transform.protobuf.IDerivative|null);
}
```

## Properties

* `name` is the name of the transform. The result of this transform an be
returned by including the name in the *outputNames* array in the transform call.

Exactly one of the following must be included.

* `identity` describes the identity transfrom
* `movingAverage` describes the moving average transform
* `derivative` describes the derivative transform
