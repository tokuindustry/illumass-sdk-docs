## Update
Update user

### Signature
```
illumass.user.update(change: UserChange)
```

### Access control
User must be signed in and must have edit permission on the user.

### Arguments

* _change_ is the [user](../../../entities/users.md) to be updated. `key` must be specified and it should
    have the form `/users/<user id>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[User](../../../entities/users.md)&gt;.
It contains the updated user.

Throws a 404 Not Found error if user with _change.key_ does not exist.
