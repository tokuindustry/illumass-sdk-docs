## List by signal key
List alarms by signal key

### Signature
```
illumass.alarm.listBySignalKey(signalKey: string)
```

### Access control
User must be signed in to access.

### Arguments

* _signalKey_ of signal to list alarms for.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Alarm](../../../entities/alarms.md)&gt;&gt;.
Alarms are returned ordered by _started_ descending *i.e.* most recent first.
