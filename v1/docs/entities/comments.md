# Comments

 Comments are notes that can be attached to alarms, users, assets, tenants,
 schemas, monitoring points, and devices.

 ```
 {
  "lastAudit": {
    "userKey": "/users/Ixln5PfUQZNAa0KR",
    "timestamp": "2019-05-20T07:45:42.914Z"
  },
  "key": "/comments/0019000000000001",
  "archived": false,
  "authorKey": "/users/Ixln5PfUQZNAa0KR",
  "linkKey": "/alarms/zvzPZSTz6UkJR0L0",
  "content": "test",
  "created": "2019-05-20T07:45:42.618Z",
  "updated": "2019-05-20T07:45:42.618Z"
}
```

## Properties

* _authorKey_ is the user who wrote the comment.
* _linkKey_ is the business object that the comment is attached to.
* _content_ is the comment text.
* _created_ is the ISO-8601 timestamp of when comment was created.
* _updated_ is the ISO-8601 timestamp of when comment was last modified.
