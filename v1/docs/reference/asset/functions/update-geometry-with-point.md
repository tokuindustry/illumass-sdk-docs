# Update geometry with point

Update asset geometry to the given point. The coordinates are expressed in
decimal degrees.

## Signature

```typescript
illumass.asset.updateGeometryWithPoint(assetKey: string,
                                       longitude: number,
                                       latitude: number)
```

## Arguments

* _assetKey_ of asset.
* _longitude_ of the coordinate.
* _latitude_ of the coordinate.

## Resolves

Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt;.
It contains the updated asset.

Throws a 404 Not Found error if asset with _assetKey_ does not exist.
