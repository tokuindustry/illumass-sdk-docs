## Create
Create comment

### Signature
```
illumass.comment.create(change: CommentChange)
```

### Access control
User must be signed in and have edit permission on linked entity.

### Arguments

* _change_ is the [comment](../../../entities/comments.md) to create. If `key` is not specified,
    it will be auto-generated. If it is specified, it should have the form
    `/comments/<client id>`. The client id should be a value that has a very
    high probability of being unique. For example
    [uuid](https://www.npmjs.com/package/uuid) v4 should be adequate, as well as
    more compact forms of ids.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Comment](../../../entities/comments.md)&gt;.
