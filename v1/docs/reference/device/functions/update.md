## Update
Update device

### Signature
```
illumass.device.update(change: DeviceChange)
```

### Access control
User must be signed in and must have admin permission on the device.

### Arguments

* _change_ is the [device](../../../entities/devices.md) to be updated. `key` must be specified and it should
    have the form `/devices/<serial number>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Device](../../../entities/devices.md)&gt;.
It contains the updated device.

Throws a 404 Not Found error if device with _change.key_ does not exist.
