<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.bundle.min.js"></script>

<script>
'use strict';

function convertChartJsBlocks() {
	const codeNodes = document.querySelectorAll('pre.chartjs code');

	for (const codeNode of codeNodes) {
		const container = document.createElement('div');
		container.className = 'chartjs';
		const canvas = document.createElement('canvas');
		container.appendChild(canvas);
		codeNode.parentNode.replaceWith(container);

		const configSrc = codeNode.innerText;
		const config = JSON.parse(configSrc);
		new Chart(canvas, config);
	}

}

if (document.addEventListener) {
	document.addEventListener("DOMContentLoaded", convertChartJsBlocks);
} else {
	document.attachEvent("onreadystatechange", function () {
		if (document.readyState === "interactive") {
			convertChartJsBlocks();
		}
	});
}
</script>

<style>
div.chartjs {
	width: 100%;
}
</style>