## Subscribe
Subscribe to a single comment

### Signature
```
illumass.comment.subscribe(commentKey: string, callback:(change: AnnotatedData<Comment>) => void)
```

### Access control
User must be signed in to access and have View permission on the linked entity.

### Arguments

* _commentKey_ of comment.
* _callback_ if function that is called every time comment is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Comment](../../../entities/comments.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).

Throws a 404 Not Found error if comment with _commentKey_ does not exist.
