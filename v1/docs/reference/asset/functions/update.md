# Update

Update asset

## Signature

```typescript
illumass.asset.update(change: AssetChange)
```

## Access control

User must be signed in and must have edit permission on the asset.

## Arguments

* _change_ is the [asset](../../../entities/assets.md) to be updated. `key` must be specified and it should
    have the form `/assets/<asset id>`.

## Resolves

Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt;.
It contains the updated asset.

Throws a 404 Not Found error if asset with _change.key_ does not exist.
