## Action context
Action context

```
interface ActionContext {
  userKey: string;
  tenantKey: string;
}
```

See [login context](../../../guides/concepts/login-context.md) for more
information.
