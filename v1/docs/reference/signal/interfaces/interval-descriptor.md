# Interval descriptor

Describes a signal interval.

```typescript
interface IntervalDescriptor {
  start: moment.Moment;
  end?: moment.Moment;
  durationInSeconds?: number;
}
```

Intervals are described with a start time and exactly one of

* an end time (`end`)
* duration in seconds (`durationInSeconds`)
