# Transform a signal

Transform slices for the given signal and interval.

## Signature

```typescript
illumass.signal.transform(
  signal: SignalDescriptor,
  interval: IntervalDescriptor,
  transforms: Transform[],
  outputNames?: string[]
)
```

## Access control

User needs to be granted view permission on the device or monitoring point of
the signal.

## Arguments

* `signal` is a [SignalDescriptor](../interfaces/signal-descriptor.md) that
describes the signal to fetch slice from.
* `interval` is a [IntervalDescriptor](../interfaces/interval-descriptor.md)
that describes the interval fetch slice from.
* `transforms` is an array of [Transform](../interfaces/starswirl-transform.md) that
describes the transforms to apply.
* `outputNames` is an optional list of transform names to export in the response.

## Resolves

Resolves to a [AnnotatedData](../../common/interfaces/annotated-data.md)
&lt;[IResponse](../interfaces/starswirl-response.md)&gt;.
