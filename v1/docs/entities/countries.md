# Countries
Represents a country. Note that the key is the [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) code prefixed with `/countries/`.

```
{
  "lastAudit": {
    "userKey": ""
  },
  "key": "/countries/US",
  "archived": false,
  "code": "US",
  "name": "United States"
}
```

## Properties

* _code_ is the [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) code.
* _name_ is the name of the country.
 
