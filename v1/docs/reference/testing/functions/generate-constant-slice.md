# Generate constant slice

Generate a constant slice for the purposes of testing.

## Signature

```typescript
illumass.testing.generateConstantSlice(
  signal: SignalDescriptor,
  interval: IntervalDescriptor,
  params: ConstantSignalParameters
)
```

## Access control

User needs to be granted admin permission on the device or monitoring point of
the signal.

## Arguments

* `signal` is a [SignalDescriptor](../../signal/interfaces/signal-descriptor.md)
that describes the signal to generate data for.
* `interval` is a [IntervalDescriptor](../../signal/interfaces/interval-descriptor.md)
that describes the interval to generate data for.
* `params` is a [ConstantSignalParameters](../interfaces/constant-signal-parameters.md)
that describes the shape of the signal.

## Resolves

Successfully resolves if the request is successful.
Note that signal data will appear 5-10 seconds afterwards. At this time there is
no way to reliably tell when the data has finished generating. This means that
for automated tests you will need to add a manual timeout/sleep.
