## Change user password
Change password of a user.

### Signature
```
illumass.auth.changeUserPassword(
  userKey: string,
  password: string
)
```

### Access control
Signed in users with administrator access on the given user can access

### Arguments

* `userKey` is user to change password for.
* `password` is the new password.

### Resolves
Resolves if password change is successful.
