## Get
Get asset role

### Signature
```
illumass.assetRole.get(assetRoleKey: string)
```

### Access control
User must be signed in to access and have View permission on _assetRoleKey_.

### Arguments

* _assetRoleKey_ of asset role.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[AssetRole](../../../entities/asset-roles.md)&gt;.

Throws a 403 Forbidden if user does not have View permission on _assetRoleKey_.
Throws a 404 Not Found error if asset role with _assetRoleKey_ does not exist.
