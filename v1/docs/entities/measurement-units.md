# Measurement Units
Measurement unit is a unit for some kind of measurement.

```
{
  "lastAudit": {
    "userKey": ""
  },
  "key": "/measurementUnits/meters",
  "name": "Meters",
  "symbol": "m",
  "measurementType": "length",
  "slope": 1,
  "offset": 0
}
```

## Properties

* _name_ of the measurement unit.
* _symbol_ of the measurement unit.
* _measurementType_ indicates what kind of quantity is being measured. It must be one of

    - `dimensionless`
    - `electricalCharge`
    - `electricalCurrent`
    - `duration`
    - `flowRate`
    - `length`
    - `pressure`
    - `signalStrength`
    - `temperature`
    - `voltage`
    - `volume`

* _slope_ and _offset_ describe how to convert back to the system preferred unit.

$$
\textit{system units} = \textit{slope} \times x + \textit{offset}
$$
