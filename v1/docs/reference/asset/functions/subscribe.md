## Subscribe
Subscribe to a single asset by key

### Signature
```
illumass.asset.subscribe(assetKey: string, callback:(change: AnnotatedData<Asset>) => void)
```

### Access control
User must be signed in to access and have View permission on _assetKey_.

### Arguments

* _assetKey_ of asset.
* _callback_ if function that is called every time asset is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
