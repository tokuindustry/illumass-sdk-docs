## Gaussian signal parameters
Parameters for a gaussian (normal) distribution signal.

```
interface GaussianSignalParameters {
  mean: number;
  variance?: number;
  standardDeviation?: number;
}
```

A gaussian distribution signal has the following parameters

* mean is the mean of the distribution
* one of
    - variance is the variance of the distribution
    - standardDeviation can be used instead of variance.
    - if neither is specified defaults to variance of 1

![Gaussian signal](gaussian-plot.jpeg)
