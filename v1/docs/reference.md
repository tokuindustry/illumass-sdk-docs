# API Reference

## Common Interfaces

* [AnnotatedData](reference/common/interfaces/annotated-data.md)
* [Page](reference/common/interfaces/page.md)
* [SearchOptions](reference/common/interfaces/search-options.md)
* [Subscription](reference/common/interfaces/subscription.md)

## Functions by Category

## Alarms

* Functions
    * [clear](reference/alarm/functions/clear.md)
    * [get](reference/alarm/functions/get.md)
    * [list](reference/alarm/functions/list.md)
    * [listBySignalKey](reference/alarm/functions/list-by-signal-key.md)
    * [update](reference/alarm/functions/update.md)
    * [subscribe](reference/alarm/functions/subscribe.md)
    * [subscribeBySignalKey](reference/alarm/functions/subscribe-by-signal-key.md)

## Assets

* Functions
    * [archive](reference/asset/functions/archive.md)
    * [create](reference/asset/functions/create.md)
    * [get](reference/asset/functions/get.md)
    * [getRoot](reference/asset/functions/get-root.md)
    * [list](reference/asset/functions/list.md)
    * [listByParentKey](reference/asset/functions/list-by-parent-key.md)
    * [subscribe](reference/asset/functions/subscribe.md)
    * [subscribeAll](reference/asset/functions/subscribe-all.md)
    * [update](reference/asset/functions/update.md)
    * [updateGeometryWithPoint](reference/asset/functions/update-geometry-with-point.md)
    * [updateGeometryWithLineString](reference/asset/functions/update-geometry-with-line-string.md)

## Asset Roles

* Functions
    * [get](reference/asset-role/functions/get.md)
    * [list](reference/asset-role/functions/list.md)

## Asset Types

* Functions
    * [get](reference/asset-type/functions/get.md)
    * [list](reference/asset-type/functions/list.md)

## Authentication

* Functions
    * [changeMyPassword](reference/auth/functions/change-my-password.md)
    * [changeUserPassword](reference/auth/functions/change-user-password.md)
    * [login](reference/auth/functions/login.md)
    * [logout](reference/auth/functions/logout.md)
    * [me](reference/auth/functions/me.md)
    * [refresh](reference/auth/functions/refresh.md)
    * [defaultLoginContext](reference/auth/functions/default-login-context.md)
    * [currentLoginContext](reference/auth/functions/current-login-context.md)
    * [setLoginContextTenantKey](reference/auth/functions/set-login-context-tenant-key.md)

## Comments

* Functions
    * [archive](reference/comment/functions/archive.md)
    * [create](reference/comment/functions/create.md)
    * [get](reference/comment/functions/get.md)
    * [list](reference/comment/functions/list.md)
    * [listByAuthorKey](reference/comment/functions/list-by-author-key.md)
    * [listByLinkKey](reference/comment/functions/list-by-link-key.md)
    * [subscribe](reference/comment/functions/subscribe.md)
    * [subscribeByAuthorKey](reference/comment/functions/subscribe-by-author-key.md)
    * [subscribeByLinkKey](reference/comment/functions/subscribe-by-link-key.md)
    * [update](reference/comment/functions/update.md)

## Countries

* Functions
    * [get](reference/country/functions/get.md)
    * [list](reference/country/functions/list.md)

## Devices

* Functions
    * [assignToTenant](reference/device/functions/assign-to-tenant.md)
    * [get](reference/device/functions/get.md)
    * [list](reference/device/functions/list.md)
    * [subscribe](reference/device/functions/subscribe.md)
    * [subscribeAll](reference/device/functions/subscribe-all.md)
    * [unassignFromTenant](reference/device/functions/unassign-from-tenant.md)
    * [update](reference/device/functions/update.md)

## Measurement Units

* Functions
    * [get](reference/measurement-unit/functions/get.md)
    * [list](reference/measurement-unit/functions/list.md)
    * [listByMeasurementType](reference/measurement-unit/functions/list-by-measurement-type.md)

## Monitoring Points

* Functions
    * [archive](reference/monitoring-point/functions/archive.md)
    * [create](reference/monitoring-point/functions/create.md)
    * [get](reference/monitoring-point/functions/get.md)
    * [getByDeviceKey](reference/monitoring-point/functions/get-by-device-key.md)
    * [list](reference/monitoring-point/functions/list.md)
    * [listByAssetKey](reference/monitoring-point/functions/list-by-asset-key.md)
    * [subscribe](reference/monitoring-point/functions/subscribe.md)
    * [subscribeByAssetKey](reference/monitoring-point/functions/subscribe-by-asset-key.md)
    * [update](reference/monitoring-point/functions/update.md)

## Server

* Functions
    * [version](reference/server/functions/version.md)

## Signals

* Interfaces
    * [IntervalDescriptor](reference/signal/interfaces/interval-descriptor.md)
    * [SignalDescriptor](reference/signal/interfaces/signal-descriptor.md)

* Functions
    * [downscaleSlice](reference/signal/functions/downscale-slice.md)
    * [exportCsv](reference/signal/functions/export-csv.md)
    * [fetchSlice](reference/signal/functions/fetch-slice.md)
    * [get](reference/signal/functions/get.md)
    * [list](reference/signal/functions/list.md)
    * [listByHolderKey](reference/signal/functions/list-by-holder-key.md)
    * [listBySignalTypeKey](reference/signal/functions/list-by-signal-type-key.md)
    * [subscribe](reference/signal/functions/subscribe.md)
    * [subscribeByHolderKey](reference/signal/functions/subscribe-by-holder-key.md)
    * [transform](reference/signal/functions/transform.md)
    * [update](reference/signal/functions/update.md)

## Signal Types

* Functions
    * [list](reference/signal-type/functions/list.md)
    * [get](reference/signal-type/functions/get.md)

## Tenants

* Functions
    * [archive](reference/tenant/functions/archive.md)
    * [create](reference/tenant/functions/create.md)
    * [get](reference/tenant/functions/get.md)
    * [list](reference/tenant/functions/list.md)
    * [subscribe](reference/tenant/functions/subscribe.md)
    * [subscribeAll](reference/tenant/functions/subscribe-all.md)
    * [update](reference/tenant/functions/update.md)

## Users

* Functions
    * [archive](reference/user/functions/archive.md)
    * [create](reference/user/functions/create.md)
    * [get](reference/user/functions/get.md)
    * [list](reference/user/functions/list.md)
    * [subscribe](reference/user/functions/subscribe.md)
    * [update](reference/user/functions/update.md)
    * [subscribeAll](reference/user/functions/subscribe-all.md)

## Testing

These interfaces and functions are used for testing. They can only be called
against test environments.

* Interfaces
    * [ConstantSignalParameters](reference/testing/interfaces/constant-signal-parameters.md)
    * [LinearSignalParameters](reference/testing/interfaces/linear-signal-parameters.md)
    * [SinusoidSignalParameters](reference/testing/interfaces/sinusoid-signal-parameters.md)
    * [GaussianSignalParameters](reference/testing/interfaces/gaussian-signal-parameters.md)

* Functions
    * [loadFixture](reference/testing/functions/load-fixture.md)
    * [generateConstantSlice](reference/testing/functions/generate-constant-slice.md)
    * [generateLinearSlice](reference/testing/functions/generate-linear-slice.md)
    * [generateSinusoidSlice](reference/testing/functions/generate-sinusoid-slice.md)
    * [generateGaussianSlice](reference/testing/functions/generate-gaussian-slice.md)
    * [updateStatus](reference/testing/functions/update-status.md)
