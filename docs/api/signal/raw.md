# Raw

Get the raw values for a signal.

## Examples

=== "Typescript"

    ```typescript
    const timeSeries = illumass.signalData.raw(ref, start, end, resolution, options);
    ```

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl -H 'Authorization: Bearer <jwt>' \
    'https://api.illumass.com/v4/signal/raw/signal!<signal id>?start=2020-08-17T19:00:00Z&duration=PT5M&format=json'
    ```

    **WebSocket**

    ```json
    {
      "action": "raw",
      "controller": "rumble/signal",
      "ref": "signal!<signal id>",
      "start": "2020-08-17T19:00:00Z",
      "duration": "PT5M",
      "format": "json"
    }
    ```

    **Response**

    ```json
    {
      "raws": {
        "timestamps": ["2020-08-17T19:00:00Z", "2020-08-17T19:01:00Z", "2020-08-17T19:02:00Z", "2020-08-17T19:03:00Z", "2020-08-17T19:04:00Z"],
        "values": [6.564000129699707, 6.564000129699707, 6.564000129699707, 6.564000129699707, 6.564000129699707]
      }
    }
    ```

## Parameters

- *ref*

    Required. [Signal entity reference](../../../entities/references#signals).

- *start*, *end*, *duration*

    Required. The interval of the signal data to retrieve. At least one of *start*
    or *end* must be specified.

    - *start* [ISO 8601 date time](https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations)
    for start of interval, inclusive.
    - *end* [ISO 8601 date time](https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations)
    for end of interval, exclusive.
    - *duration* [ISO 8601 duration](https://en.wikipedia.org/wiki/ISO_8601#Durations).
    Duration is consided only if one of *start* or *end* is missing. Default to
    1 hour.

- *resolution*

    Optional. Resolution of signal data to get. Must be one of *hour*, *minute*,
    or *second*. If not specified, the signal default is used.

- *measuringUnit*

    Optional. Can be one of *source*, *tenant*, or a *measuring-unit* entity reference.

    - If *source*, then uses the signal default.
    - If *tenant*, then converts to the measuring unit configured for the tenant.
    - If a *measuring-unit* entity reference, then converts to the specified
    measuring unit.

    Defaults to *source*.

- *format*

    Optional. Format the signal data.

    - *binary*. Data is encoded as protobuf binary.
    - *base64*. Data is encoded as base64, protobuf binary.
    - *json*. Data is encoded as two json arrays. One array is the timestamps,
    the other is the signal values.

        ```json
        {
          "raws": {
            "timestamps": […],
            "values": […],
          }
        }
        ```

    - *csv*. Data is encoded as a csv with two colums.
    The first column is the timestamp, the second is the signal value.

    Defaults to *base64*.

- *datetime*

    Optional. When *format* is *json* or *csv*, this specifies the format to use
    for timestamps. The valid values are

    - *iso*, *iso8601*. Timestamps are formatted as [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).
    - *excel*, *csv*. Use Excel friendly timestamp format YYYY-MM-DD HH:mm:ss.
    - *milliseconds*, *millisecond*, *ms*. Milliseconds since 1970-01-01T00:00Z.
    Useful for programming languages that represent time as millseconds.
    e.g. JavaScript, Java
    - *seconds*, *second*, *s*. Seconds since 1970-01-01T00:00Z.
    Useful for programming languages that represent time as seconds.
    e.g. Python, R

    Defaults to *iso8601*

- *timezone*

    Optional. When *format* is *json* or *csv*, this specifies the [tz database name](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
    to use for timestamps. Defaults to *UTC*.
