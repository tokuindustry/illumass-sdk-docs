# Signal descriptor

Describes a signal in the system.

```typescript
interface SignalDescriptor {
  signalKey?: string;
  serialNumber?: string;
  holderKey?: string;
  signalTypeKey?: string;
}
```

A signal can be described with exactly one of the following.

* a signal key (`signalKey`)
* a device serial number (`serialNumber`) AND a signal type key (`signalTypeKey`)
* a holder key (`holderKey`) AND a signal type key (`signalTypeKey`). A holder
key must be a device or monitoring point key.
