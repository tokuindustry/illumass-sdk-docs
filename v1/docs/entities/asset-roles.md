# Asset Roles

An asset role defines a user group for a given asset.

```
{
  "lastAudit": {
    "userKey": ""
  },
  "key": "/assetRoles/0014000000000006",
  "archived": false,
  "name": "Viewers"
}
```

## Properties

* _name_ of the asset role

At this time 3 asset roles are automatically create for every tenant.

* _Viewers_

    Viewers have view permission on this asset and every sub-asset under it.
    They do not receive alarm notifications.
    <br/><br/>

* _Operators_

    Operators have view permission and can clear alarms on this asset and
    every sub-asset under it. Operators will receive alarm notifications.
    <br/><br/>

* _Managers_

    Managers have write permission and can clear alarms on this asset and
    every sub-asset under it. Managers will receive alarm notifications.
    <br/><br/>
