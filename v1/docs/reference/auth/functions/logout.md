## Logout
Logout

### Signature
```
illumass.auth.logout()
```

### Access control
Signed in users can call this.

### Arguments

* `email` is the user's email
* `password` is the user's password

### Resolves
Resolves if logout succeesds.
