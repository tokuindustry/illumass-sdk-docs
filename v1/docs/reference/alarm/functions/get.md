## Get
Get alarm

### Signature
```
illumass.alarm.get(alarmKey: string)
```

### Access control
User must be signed in to access and have View permission on signal's holder.

### Arguments

* _alarmKey_ of the alarm

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Alarm](../../../entities/alarms.md)&gt;.

Throws a 403 Forbidden if user does not have View permission on _alarmKey_.
Throws a 404 Not Found error if alarm with _alarmKey_ does not exist.
