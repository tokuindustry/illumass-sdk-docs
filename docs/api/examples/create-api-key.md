# Create an API key

Before using the API you will need to an obtain an API key and token. This can
be done via the webapp or through API calls.

## Web app

!!! important
    This feature is still in early beta. The final release may be different.

1. Open your browser and go to <https://beta.illumass.com>.

    !!! Note
        Although the web app should work on most browsers, currently only
        Chrome is officially supported.

    ![Login](../images/login.png)

2. After signing in, open the side bar and click on *Manage API keys*.

    ![Sidebar](../images/sidebar-manage-api-keys.png)

3. Click to add an API key.

    ![Add API key](../images/manage-api-keys-add.png)

4. Enter a suitable description and choose an expiration.

    ![API key description](../images/manage-api-keys-description.png)

5. The API key and token will be created.

    !!! Attention
        This is the only time the token will be shown, so make sure you copy it
        and save it in a secure location. If the token becomes compromised or
        you no longer need it, you can revoke it from the *Manage API keys* screen.

    ![Copy token](../images/manage-api-keys-copy-token.png)

## API calls

You can also create an API key using the API.

1. Login and get the JWT from the response

    ```bash
    curl --data '{ "email": "<email>", "password": "<password>" }' \
    -H 'Content-Type: application/json' \
    https://api.illumass.com/v4/login?expiresIn=1h
    ```

    **Response**

    ```json
    {
      "requestId": "8d8671a1-2db6-43f9-ab00-73a67d6e228f",
      "status": 200,
      "error": null,
      "controller": "auth",
      "action": "login",
      "collection": null,
      "index": null,
      "volatile": null,
      "result": {
        "_id": "0005000000000008",
        "expiresAt": 1597687105283,
        "jwt": "<jwt>",
        "ttl": 3600000
      }
    }
    ```

2. Create API key. Include the *description* and *expiresIn*.
    Make sure you copy and save the API key JWT in the response.
    This is the only time it will be returned by the server.

    ```bash
    curl -H 'Authorization: Bearer <jwt>' \
    -H 'Content-Type: application/json' \
    --data '{ "description": "<description>" }' \
    'https://api.illumass.com/v4/api-keys/_create?expiresIn=1h'
    ```

    **Response**

    ```json
    {
      "requestId": "ba0c7537-3c61-45f0-a798-de844284570e",
      "status": 200,
      "error": null,
      "controller": "auth",
      "action": "createApiKey",
      "collection": null,
      "index": null,
      "volatile": null,
      "result": {
        "_id": "oOJ9CHQB7KmymBNZ3uuL",
        "_source": {
          "description": "<description>",
          "expiresAt": 1597873900806,
          "fingerprint": "<jwt fingerprint>",
          "token": "<api key jwt>",
          "ttl": 3600000,
          "userId": "0005000000000008",
          "_kuzzle_info": {
            "author": null,
            "createdAt": 1597870300809,
            "updatedAt": null,
            "updater": null
          }
        }
      }
    }
    ```
