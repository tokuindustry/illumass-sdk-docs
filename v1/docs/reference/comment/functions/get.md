## Get comment
Get comment

### Signature
```
illumass.comment.get(commentKey: string)
```

### Access control
User must be signed in to access and have View permission on linked entity.

### Arguments

* _commentKey_ of comment.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Comment](../../../entities/comments.md)&gt;.

Throws a 403 Forbidden if user does not have View permission on linked entity.
Throws a 404 Not Found error if comment with _commentKey_ does not exist.
