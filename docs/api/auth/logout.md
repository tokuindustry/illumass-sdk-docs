# Logout

Logout the currentlyl logged in user.

=== "Typescript"

    ```typescript
    await illumass.auth.logout();
    ```

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl -X POST \
    -H 'Authorization: Bearer <jwt>' \
    https://api.illumass.com/v4/logout
    ```

    **WebSocket**

    ```json
    {
      "controller": "auth",
      "action": "logout",
      "jwt": "<jwt>",
    }
    ```

    **Response**

    ```json
    {
      "requestId": "6494cfa5-eabe-4726-abae-970d64473bb1",
      "status": 200,
      "error": null,
      "controller": "auth",
      "action": "logout",
      "collection": null,
      "index": null,
      "volatile": null,
      "result": {
        "acknowledged": true
      }
    }
    ```
