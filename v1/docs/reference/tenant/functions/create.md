## Create
Create tenant

### Signature
```
illumass.tenant.create(change: TenantChange)
```

### Access control
User must be signed in and be a system admin or customer support user.

### Arguments

* _change_ is the [tenant](../../../entities/tenants.md) to create. If `key` is not specified,
    it will be auto-generated. If it is specified, it should have the form
    `/tenants/<client id>`. The client id should be a value that has a very
    high probability of being unique. For example
    [uuid](https://www.npmjs.com/package/uuid) v4 should be adequate, as well as
    more compact forms of ids.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Tenant](../../../entities/tenants.md)&gt;.
