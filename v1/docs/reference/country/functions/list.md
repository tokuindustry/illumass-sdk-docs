## List
List countries

### Signature
```
illumass.country.list()
```

### Access control
User must be signed in to access.

### Arguments

There are no arguments.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Country](../../../entities/countries.md)&gt;&gt;.
