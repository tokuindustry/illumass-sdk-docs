## Unassign from tenant
Unassign devices

### Signature
```
illumass.device.unassignFromTenant(deviceKeys: string[])
```

### Access control
User must be signed in to access and
* have admin permission on _tenantKey_ of each device
* have admin permission on all _deviceKeys_

### Arguments

* _deviceKeys_ devices to unassign from tenant

### Resolves
Resolves if successful.
