## Subscribe
Subscribe to a single tenant by key

### Signature
```
illumass.tenant.subscribe(tenantKey: string, callback:(change: AnnotatedData<Tenant>) => void)
```

### Access control
User must be signed in to access and have View permission on _tenantKey_.

### Arguments

* _tenantKey_ of tenant.
* _callback_ if function that is called every time tenant is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Tenant](../../../entities/tenants.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).

Throws a 404 Not Found error if tenant with _tenantKey_ does not exist.
