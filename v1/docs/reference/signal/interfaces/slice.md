# Slice

Slice of a signal

```typescript
interface Slice {
  startMsecs: number|Long;
  secondsIncrement: number[];
  reading: number[];
}
```

## Properties

* `startMsecs` is the time of the start time of the slice in milliseconds since
the epoch (1970-01-01T00:00:00.000Z).
* `secondsIncrement` is an array of numbers indicating the number of seconds
since the previous reading.
* `reading` is an array of numbers indicating the readings in the slice.

## Example

Consider the following 5 second slice.

```typescript
{
  startMsecs: 1557100800034,
  secondsIncrement: [0, 1, 1, 1, 1],
  reading: [100, 101, 102, 103, 104]
}
```

The slice starts at `2019-05-06T00:00:00Z`, since this is `1557100800034` ms
after the epoch.

The first reading, 100, has a timestamp of `2019-05-06T00:00:00Z`.
Note that its corresponding `secondsIncrement` is 0.
The timestamp is calculated as the last timestamp, which in this case is the same
as the start of the slice `2019-05-06T00:00:00Z`, plus 0 seconds.

The timestamp for the second reading, 101, we add the corresponding
`secondsIncrement`, 1, to the previous timestamp `2019-05-06T00:00:00Z` to get `2019-05-06T00:00:01Z`.

Similarly, we calculate a timestamp of `2019-05-06T00:00:02Z` for a reading of 102,
`2019-05-06T00:00:03Z` for 103, and `2019-05-06T00:00:04Z` for 104.
