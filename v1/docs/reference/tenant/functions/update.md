## Update
Update tenant

### Signature
```
illumass.tenant.update(change: TenantChange)
```

### Access control
User must be signed in and must have admin permission on the tenant.

### Arguments

* _change_ is the [tenant](../../../entities/tenants.md) to be updated. `key`
  must be specified and it should have the form `/tenants/<tenant id>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Tenant](../../../entities/tenants.md)&gt;.
It contains the updated tenant.

Throws a 404 Not Found error if tenant with _change.key_ does not exist.
