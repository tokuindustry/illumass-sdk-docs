## List
List monitoring points that user has permission to view.

This function is less useful, as a monitoring point by itself is only meaningful
within the context of the asset it belongs to. However, it is included for
completeness.

### Signature
```
illumass.monitoringPoint.list()
```

### Access control
User must be signed in to access.

### Arguments

There are no arguments.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MonitoringPoint](../../../entities/monitoring-points.md)&gt;&gt;.
