# Slice collection

A collection of slices.

```typescript
interface ISliceCollection {

    /** SliceCollection slices */
    slices?: (toku.starswirl.protobuf.ISlice[]|null);
}
```

## Properties

* `slices` is a list of [slices](./starswirl-slice.md) in this collection
