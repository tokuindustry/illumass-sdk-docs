## Get
Get monitoring point

### Signature
```
illumass.monitoringPoint.get(monitoringPointKey: string)
```

### Access control
User must be signed in to access and have View permission on moitoring point asset.

### Arguments

* _monitoringPointKey_ of monitoring point.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MonitoringPoint](../../../entities/monitoring-points.md)&gt;.

Throws a 403 Forbidden if user does not have View permission on _monitoringPointKey_.
Throws a 404 Not Found error if monitoring point with _monitoringPointKey_ does not exist.
