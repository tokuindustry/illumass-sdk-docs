## Change my password
Change password of current user.

### Signature
```
illumass.auth.changeMyPassword(
  nextPassword: string
)
```

### Access control
Signed in users can access

### Arguments

* `nextPassword` is the user's new password.

### Resolves
Resolves if password change is successful.
