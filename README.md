# Illumass SDK docs

## Running with Python

1. Setup [pyenv](https://github.com/pyenv/pyenv).
1. Change working directory to directory of this `README.md` file.
1. Install required version of python.

	```bash
	pyenv install
	```

1. Setup [venv](https://docs.python.org/3/library/venv.html).

	```bash
	python -m venv .venv
	```

1. Activate virtual environment.

	```bash
	source .venv/bin/activate
	```

	Use `deactivate` from the command line to deactivate the virutal environment.

1. Install dependencies

	```bash
	pip install -r requirements.txt
	```

1. Start mkdocs.

	```bash
	mkdocs serve
	```

1. [Open browser](http://localhost:8000)

## Bitbucket Setup for Windows

1. Install Git and VS Code or Sourcetree
	- [Git Windows](https://git-scm.com/downloads)
	- [Visual Studio Code](https://code.visualstudio.com)
	- [Sourcetree](https://www.sourcetreeapp.com)
2. Open Bitbucket in browser and open selected repo
3. Select clone and choose whether to use VS Code or Sourcetree
4. Clone to local repo

## mkdocs Setup for Windows with Anaconda/Conda Virtual Environment

1. Create new Conda virtual environment

	```bash
	conda create –name myenv python=3.8.5
	```

2. Activate environment

	```bash
	conda activate myenv
	```

3. Install dependencies for mkdocs

	```bash
	pip install -r requirements.txt
	```

4. Start mkdocs

	```bash
	mkdocs serve
	```

## Tools

- [Docker](https://docker.com) is used for development and deployment
- [nvm.sh](https://nvm.sh) is used to manage verion of NodeJS to use in a project.
- Install [Visual Studio Code](https://code.visualstudio.com)
	- Extensions
		Some useful extensions that I use.
		- Productivity/collaboration
			- [ms-vsliveshare.vsliveshare-pack](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare-pack)
			- [bierner.markdown-shiki](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-shiki)
			- [bierner.markdown-mermaid](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid)
			- [goessner.mdmath](https://marketplace.visualstudio.com/items?itemName=goessner.mdmath)
			- [ms-azuretools.vscode-docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
			- [abumalick.vscode-nvm](https://marketplace.visualstudio.com/items?itemName=abumalick.vscode-nvm)
		- Programming
			- [alexkrechik.cucumberautocomplete](https://marketplace.visualstudio.com/items?itemName=alexkrechik.cucumberautocomplete)
			- [DavidAnson.vscode-markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
			- [dbaeumer.vscode-eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
			- [Orta.vscode-jest](https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest)
			- [esbenp.prettier-vscode](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
		- Data science
			- [ms-toolsai.jupyter](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter)

## Services

- [Docker](https://docker.com)
- [Bitbucket](https://bitbucket.org)
