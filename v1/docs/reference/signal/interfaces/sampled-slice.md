# Sampled slice

A slice that has been downscaled to samples with a given duration.

```typescript
interface SampledSlice {
  startMsecs: number|Long;
  durationInSeconds: number;
  mean: number[];
  variance: number[];
  min: number[];
  max: number[];
}
```

## Properties

* *startMsecs* is the time of the start time of the slice in milliseconds since
the epoch (1970-01-01T00:00:00.000Z).
* *durationInSeconds* is the duration of each sample in seconds.
* statistical *mean* for each sample
* statistical *variance* of each sample
* *min* of each sample
* *max* of each sample

For example, suppose we have *sampledSlice*, which has

* *sampledSlice.startMsecs = 1562616000000*, or Monday, July 8, 2019 20:00:00 UTC.
* *sampledSlice = 60*, meaning each sample represents a minute.
* *sampledSlice.mean[0]* is the mean of the first minute.
* *sampledSlice.variance[0]* is the variance of the first minute.
* *sampledSlice.min[0]* is the min value of the first minute.
* *sampledSlice.max[0]* is the max value of the first minute.
