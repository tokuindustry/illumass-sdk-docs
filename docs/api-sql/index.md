# API-SQL

The API-SQL service was introduced to help users integrate data from Illumass
with existing systems. A database is provided that develoeprs and
third party software can access using SQL.

```mermaid
graph LR
	api[Illumass API]
	repl[Replicator]
	tsdb[(SQL Database)]
	apps[Applications]
	dbs[Databases]
	api --> repl
	subgraph API-SQL
	repl --> tsdb
	end
	subgraph Customer network
	tsdb -->dbs
	tsdb -->apps
	end
```

- Hosted, dedicated database server
- Access to time-series data using SQL
- Database access is password protected.
- Database connections are SSL encrypted.
- Data is pushed from API to SQL database in real-time.
- 1 year data retention

--8<-- "mermaid.txt"
