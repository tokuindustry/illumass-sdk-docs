## Subscription
Represents a subscription.

```
interface Subscription {
  readonly unsubscribe: () => Promise<void>;
}
```

### Methods
* _unsubscribe()_ unsubscribe from subscription. Resolves if unsubscribe is successful.
