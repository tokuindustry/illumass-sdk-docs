## List
List measurement units by measurement type

### Signature
```
illumass.measurementUnit.listByMeasurementType(measurementType: string)
```

### Access control
User must be signed in to access.

### Arguments

* _measurementType_ is one of

    - `dimensionless`
    - `electricalCharge`
    - `electricalCurrent`
    - `duration`
    - `flowRate`
    - `length`
    - `pressure`
    - `signalStrength`
    - `temperature`
    - `voltage`
    - `volume`


### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MeasurementUnit](../../../entities/measurement-units.md)&gt;&gt;.
