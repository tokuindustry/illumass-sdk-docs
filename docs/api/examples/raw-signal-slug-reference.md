# Raw signal by slug reference

In this example we show an example of getting a raw signal. A *raw* signal has
all the original values collected by the sensor.

This assumes you already have a JWT token, either via [logging in](../auth/login)
or [API key](../create-api-key).

## Slug reference

Suppose the fictional company *Acme Inc.*. It has

- An asset named *10-15-038-08W6*.
- *10-15-038-08W6* has an hardpoint called *Riser* where a sensor can be installed.
- A T1000 pressure sensor has been installed on *Riser* and is sampling every
5 seconds (i.e. 0.2 Hz).

The pressure signal can then be referenced with the slug *signal.acme-inc.10-15-038-08W6.riser.pressure*.

## Getting the signal

To get 1 minute of pressure data from August 28, 2020 00:00 UTC for the *Riser*,
we can use the following call.

### Request

```bash
curl -H 'Authorization: Bearer <jwt>' \
'https://api.illumass.com/v4/signal/raw/signal.acme-inc.10-15-038-08W6.riser.pressure?start=2020-08-28T00:00:00Z&duration=PT1M&format=json'
```

### Response

```json
{
	"raws": {
		"timestamps": [
      "2020-08-28T00:00:00Z",
      "2020-08-28T00:00:05Z",
      "2020-08-28T00:00:10Z",
      "2020-08-28T00:00:15Z",
      "2020-08-28T00:00:20Z",
      "2020-08-28T00:00:25Z",
      "2020-08-28T00:00:30Z",
      "2020-08-28T00:00:35Z",
      "2020-08-28T00:00:40Z",
      "2020-08-28T00:00:45Z",
      "2020-08-28T00:00:50Z",
      "2020-08-28T00:00:55Z"
    ],
		"values": [
      812.760009765625,
      813.6799926757812,
      812.8699951171875,
      812.4500122070312,
      811.8400268554688,
      812.739990234375,
      812.4099731445312,
      811.780029296875,
      812.6199951171875,
      811.72998046875,
      811.4099731445312,
      812.22998046875
    ]
	}
}
```
