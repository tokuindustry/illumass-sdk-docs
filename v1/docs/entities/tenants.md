# Tenants
Tenants (aka Customers, Accounts) usually represent an organization that is using Illumass.

```
{
  "lastAudit": {
    "userKey": "",
    "timestamp": "2019-05-08T16:14:44.444Z"
  },
  "key": "/tenants/Hee7fe9Ttvg6XXcK",
  "archived": false,
  "name": "ACME Inc.",
  "props": {},
  "countryKey": "/countries/CA",
  "pressureUnitKey": "/measurementUnits/kiloPascals",
  "temperatureUnitKey": "/measurementUnits/celsius",
  "lengthUnitKey": "/measurementUnits/meters",
  "flowRateUnitKey": "/measurementUnits/cubicMetersPerHour"
}
```

## Properties

* _name_ is the name of the tenant.
* _countryKey_ is the country the tenant is based on.
  This is typically the country the head or field office is located in.
* _pressureUnitKey_ is the pressure unit to use when displaying pressure values.
* _temperatureUnitKey_ is the temperature unit to use when displaying temperature values.
* _lengthUnitKey_ is the length unit to use when displaying length values.
* _flowRateUnitKey_ is the flow rate unit to use when displaying flow rate values.
