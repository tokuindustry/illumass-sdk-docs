## Subscribe
Subscribe to a single alarm

### Signature
```
illumass.alarm.subscribe(alarmKey: string, callback:(change: AnnotatedData<Alarm>) => void)
```

### Access control
User must be signed in to access and have View permission on the holder.

### Arguments

* _alarmKey_ of alarm.
* _callback_ if function that is called every time alarm is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Alarm](../../../entities/alarms.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).

Throws a 404 Not Found error if alarm with _alarmKey_ does not exist.
