## List
List assets that user has View permission for.

### Signature
```
illumass.asset.list()
```

### Access control
User must be signed in to access.

### Arguments

There are no arguments.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt;&gt;.
