## Get
Get country

### Signature
```
illumass.country.get(countryKey: string)
```

### Access control
User must be signed in to access.

### Arguments

* _countryKey_ of country

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Country](../../../entities/countries.md)&gt;.
