# Search

Search for entities.

## Examples

=== "Typescript"

    ```typescript
    const country = await illumass.country.search({
      query: {
        match: {
          name: "Canada"
        }
      }
    });
    ```

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl -H 'Authorization: Bearer <jwt>' \
    -H 'Content-Type: application/json' \
    --data '{ "query": { "match": { "name": "canada "}}}' \
    'https://api.illumass.com/v4/entity/<entity type>/search[?size=<int>]'
    ```

    **WebSocket**

    ```json
    {
      "action": "search",
      "controller": "document",
      "collection": "countries",
      "body": {
        "query": {
          "match": {
            "name": "canada"
          }
        }
      },
    }
    ```

    **Response**

    ```json
    {
      "requestId": "73a7416f-1ba6-4242-a003-c587932c2f43",
      "status": 200,
      "error": null,
      "controller": "document",
      "action": "search",
      "collection": "countries",
      "index": "rumble",
      "volatile": null,
      "result": {
        "hits": [{
          "_id": "EQwyrNETqxn-u-XkD1KkUQ7WPYc",
          "_score": 6.1212697,
          "_source": {
              "_kuzzle_info": {
                  "author": null,
                  "createdAt": 1595621884365,
                  "updatedAt": null,
                  "updater": null
              },
              "entityType": "country",
              "id": "EQwyrNETqxn-u-XkD1KkUQ7WPYc",
              "modified": "2020-07-24T19:38:24.007Z",
              "modifier": {
                  "entityType": "user",
                  "id": "v76O3UhDxPcebRBmXU7mi_i-TME",
                  "slug": "toku-systems.superuser",
                  "shortName": "Superuser"
              },
              "slug": "ca",
              "allowTenantIds": ["*"],
              "name": "Canada",
              "code": "CA"
          }
        }],
        "total": 1
      }
    }
    ```

## Parameters

- *entity type*

    Required. Hyphenated, plural form of entity type.

- *from*

    Optional. Show results starting from the given index.

- *size*

    Optional. Page size.

- *body*

    Required. Query is in [Elasticsearch DSL](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html).

## Additional query examples

### List all assets

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl -H 'Authorization: Bearer <jwt>' \
    -H 'Content-Type: application/json' \
    --data '{}' \
    'https://api.illumass.com/v4/entity/assets/search'
    ```
