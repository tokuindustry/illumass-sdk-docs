# Update geometry with line string

Update asset geometry to the given line string. Coordinates are expressed in
decimal degrees.

## Signature

```typescript
illumass.asset.updateGeometryWithLineString(assetKey: string, coordinates: Array<Array<number>>)
```

## Arguments

* _assetKey_ of asset.
* _coordinates_ of the line string. Each coordinate is an array of 2 numbers.
  The first number is the longitude and the second is the latitude.
  
  *e.g.* [[106.4143532, 10.7553405],[105.8194112, 21.0227788]] is the line string
  from Ho Chi Minh City to Hanoi.

## Resolves

Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt;.
It contains the updated asset.

Throws a 404 Not Found error if asset with _assetKey_ does not exist.
