# Version

Get server version

## Signature

```typescript
illumass.server.version()
```

## Access control

This call is publicly available. No sign in is required.

## Resolves

Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;string&gt;.

The version string follows the [semantic versioning convention](https://semver.org).
It must be compatible withthe [npm semver implementation](https://www.npmjs.com/package/semver).

_e.g._ _2019.3.0_
