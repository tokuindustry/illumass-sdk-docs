## Archive
Archive a user

### Signature
```
illumass.user.archive(userKey: string)
```

### Access control
User must be signed in and must have admin permission on the user.

### Arguments

* _userKey_ is the user to be archived. It should have the
    form `/users/<user id>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[User](../../../entities/users.md)&gt;.
It contains the user before it was archived.

Throws a 404 Not Found error if user with _userKey_ does not exist.
