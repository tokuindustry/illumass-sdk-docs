## List by asset key
List monitoring points by asset key.

### Signature
```
illumass.monitoringPoint.listByAssetKey(assetKey: string)
```

### Access control
User must be signed in to access and have view permission on asset.

### Arguments

* assetKey_ is the asset to list monitoring points for. Is should be have the
  form `/assets/<asset id>`.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MonitoringPoint](../../../entities/monitoring-points.md)&gt;&gt;.
