## Me
Get the currently signed in user.

### Signature
```
illumass.auth.me()
```

### Access control
Signed in users can call this.

### Arguments

There are no arguements

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[User](../../../entities/users.md)&gt;.
The user returned is the signed in user.
