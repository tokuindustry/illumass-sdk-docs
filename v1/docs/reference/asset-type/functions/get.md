## Get
Get asset type

### Signature
```
illumass.assetType.get(assetTypeKey: string)
```

### Access control
User must be signed in to access and have View permission on _assetTypeKey_.

### Arguments

* _assetTypeKey_ of asset type.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[AssetType](../../../entities/asset-types.md)&gt;.

Throws a 403 Forbidden if user does not have View permission on _assetTypeKey_.
Throws a 404 Not Found error if asset type with _assetTypeKey_ does not exist.
