## List
List Devices that user has View permission for.

### Signature
```
illumass.device.list(options?: SearchOptions)
```

### Access control
User must be signed in to access.

### Arguments

* *options*. See [SearchOptions](../../common/interfaces/search-options.md).

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Device](../../../entities/devices.md)&gt;&gt;.
