## Subscribe
Subscribe to a single monitoring point by key

### Signature
```
illumass.monitoringPoint.subscribe(monitoringPointKey: string, callback:(change: AnnotatedData<MonitoringPoint>) => void)
```

### Access control
User must be signed in to access and have View permission on monitoring point asset.

### Arguments

* _monitoringPointKey_ of monitoring point.
* _callback_ if function that is called every time asset is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MonitoringPoint](../../../entities/monitoring-points.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
