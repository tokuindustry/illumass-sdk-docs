# Signals

A signal is a [time series](https://en.wikipedia.org/wiki/Time_series) that is
associated with a device or monitoring point.

In the case of devices, it usually represents data being collected by a sensor.
The sensor could be collecting data about a physical process (e.g. pressure,
temperature, etc.), or ancillary statistics about the operation of the device
(e.g. battery voltage, signal strength, network status code, etc.)

In the case of monitoring points, it represents the data collected from
devices installed on it (see [install points](http://localhost:8000/entities/monitoring-points/#install-points)),
or a calculated value like [differential](http://localhost:8000/entities/monitoring-points/#differential)
or [multiply and delay](http://localhost:8000/entities/monitoring-points/#multiply-and-delay).

```json
{
  "lastAudit": {
    "userKey": "",
    "timestamp": "2019-05-17T22:01:02.042Z"
  },
  "key": "/signals/0003000000000002",
  "archived": false,
  "holderKey": "/devices/TEST-TFW2XK",
  "signalTypeKey": "/signalTypes/staticPressureSensorTemperature",
  "alarmConfig": {
    "low": {
      "enabled": false,
      "limit": 0,
      "hysteresis": 10,
      "setTime": 10,
      "clearTime": 10
    },
    "high": {
      "enabled": false,
      "limit": 0,
      "hysteresis": 10,
      "setTime": 10,
      "clearTime": 10
    },
    "lowLow": {
      "enabled": false,
      "limit": 0,
      "hysteresis": 10,
      "setTime": 10,
      "clearTime": 10
    },
    "highHigh": {
      "enabled": false,
      "limit": 0,
      "hysteresis": 10,
      "setTime": 10,
      "clearTime": 10
    }
  }
}
```

## Properties

* _holderKey_ is the device or monitoring point that the signal is associated
  with. It will have the form `/devices/<serial number>` or
  `/monitoringPoints/<monitoring point id>`.
* _signalTypeKey_ is the [signal type](./signal-types.md). It describes the
  kind of data being represented. (e.g. pressure, temperature, voltage, etc.)
* _alarmConfig_ describes the line level alarm set on the signal. Whenever
  signal data is reported by the device, it will check if an alarm is triggered.

    There are 4 line level alarms:

    * _low_ will trigger an alarm with a **warning** severity if the signal falls
    below the _limit_.
    * _high_ will trigger an alarm with a **warning** severity if the signal rises
    above the _limit_.
    * _lowLow_ will trigger an alarm with a **critical** severity if the signal falls
    below the _limit_.
    * _highHigh_ will trigger an alarm with a **critical** severity if the signal
    rises above the _limit_.

    Each line level alarm is configured with:

    * _enabled_ is true if line level alarm is to be applied.
    * _limit_ is the value in system standard units to check if a signal crosses.
    * _hysteresis_ influences when a signal is determined to have crossed back
    the limit. Together with the _limit_, it defines the *alarming zone*.
    It is expressed in system standard units.
    * _setTime_ is the time in seconds the signal must remain in the
    *alarming zone* before
    an alarm is triggered.
    * _clearTime_ is the time in seconds the signal must be outside of the
    *alarming zone*
    before an alarm is automatically cleared.

    Note that for the limits of enabled line levels, the following must be true

    $$
    limit_{lowLow} < limit_{low} < limit_{high} < limit_{highHigh}
    $$

## Triggering an alarm

To trigger an alarm, a signal must remain in the *alarming zone* for _setTime_
seconds **after** initially crossing the _limit_.

For _high_ and _highHigh_ line levels, the *alarming zone* is defined as

$$
  y \ge limit - hysteresis
$$

For _low_ and _lowLow_ line levels, the *alarming zone* is defined as

$$
  y \le limit + hysteresis
$$

For example, consider a high alarm with $limit = 50$ and $hysteresis = 5$
with the signal below.

![High alarm example](./images/high-alarm.png)

In this example the signal first crosses the limit at $t = 4$. Furthermore, it
falls outside the shaded alarming zone at $t = 25$. This means that for the
alarm to trigger we must have $0 \le setTime \le 20$.

The inverse example with a low alarm with $limit = 10$ and $hysteresis = 5$
would look as follows.

![Low alarm example](./images/low-alarm.png)

In this case the signal first crosses the limit at $t = 4$. It then rises
outside the shaded alarming zone at $t = 25$. Again, we must have
$0 \le setTime \le 20$ for the alarm to trigger.

--8<--
mathjax.txt
--8<--
