# Login context

A login context consists of a user key and a tenant key. Together they represent
the user who is signed in, and the tenant they are working on.

When a command is executed on the system, it checks that the user has permission
to perform the operation.

The tenant is typically used to restrict the scope of the command to only data
that belongs to the tenant. This is used by &ast;.list&ast; and
&ast;.subscribe&ast; functions to restrict results to that tenant. There may
be other functions that use the tenant key to restrict results.

## Creation
A login context is created when a user signs in to the system. The user key is
the signed in user's key, while the tenant key is set to the tenant the user
belongs to.

Note that while a user can only belong to a single tenant, but can have
permissions in many tenants. An example of this would be the Customer Support
and Auditor users. Typically they will belong to the TOKU Industry tenant, but
will have permission to access data from any tenant.

## Switching tenants
Customer Support users have the ability to change which tenants they are working
on. This is done by changing the tenant key in the login context.

To change the tenant key, specify `request.volatile.illumass.tenantKey`. For
example, the following of a request to list all the assets for for ACME.

```javascript
const request = {
  action: 'list',
  controller: 'illumass/entity',
  entityType: 'assets',
  volatile: {
    illumass: {
      tenantKey: '/tenants/<ACME tenant id>'
    }
  }
};
```

## Getting ready for Impersonation
An upcoming important use case will be impersonation. The system will allow one
user, the _impersonator_, to  perform a task on behalf of another user, the
_impersonatee_.

Some initial thoughts on implementing this would be to allow
`request.volatile.illumass.userKey` to be specified. The system would check
that the impersonator has admin permission (or some other yet to be determined
permission) on the impersonator. Furthermore, the login context would have an
additional impersonator key.

For the purposes of auditing, both the impersonator and impersonatee will need
to be stored with every audit record.
