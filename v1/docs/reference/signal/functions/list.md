## List
List signals.

This function is probably not very useful. However, it is included for
completeness.

### Signature
```
illumass.signal.list()
```

### Access control
User must be signed in to access.

### Arguments

There are no arguments.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Signal](../../../entities/signals.md)&gt;&gt;.
