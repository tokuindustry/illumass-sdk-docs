## Assign to tenant
Assign devices to a tenant

### Signature
```
illumass.device.assignToTenant(tenantKey: string, deviceKeys: string[])
```

### Access control
User must be signed in to access and
* have admin permission on _tenantKey_
* have admin permission on all _deviceKeys_

### Arguments

* _tenantKey_ of tenant to assign devices to
* _deviceKeys_ devices to assign to tenant

### Resolves
Resolves if successful.
