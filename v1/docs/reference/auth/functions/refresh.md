## Refresh
Refresh the current web token

### Signature
```
illumass.auth.refresh(expiresIn?: string)
```

### Access control
Signed in users can call this.

### Arguments

* `expiresIn` is an optional string that expresses the expiration in [ms library](https://www.npmjs.com/package/ms) format. (e.g. `2h`)


### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;string&gt;,
where the string is a new [JSON Web Token](https://jwt.io).
