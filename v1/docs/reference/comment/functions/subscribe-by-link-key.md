## Subscribe by link key
Subscribe to comments for a given entity.

### Signature
```
illumass.comment.subscribeByLinkKey(linkKey: string, callback:(change: AnnotatedData<Comment>) => void)
```

### Access control
User must be signed in to access and have View permission on the linked entity.

### Arguments

* _linkKey_ of entity.
* _callback_ if function that is called every time comment is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Comment](../../../entities/comments.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
