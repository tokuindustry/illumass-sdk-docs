## Subscribe by holder key
Subscribe to all signals of a holder.

### Signature
```
illumass.signal.subscribeByHolderKey(holderKey: string, callback:(change: AnnotatedData<Signal>) => void)
```

### Access control
User must be signed in to access.

### Arguments

* _holderKey_ is the device or monitoring point associated with the signal.
* _callback_ if function that is called every time a signal is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Signal](../../../entities/signals.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
