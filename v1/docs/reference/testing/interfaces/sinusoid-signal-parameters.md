## Sinusoid signal parameters
Parameters for a sinusoid signal.

```
interface SinusoidSignalParameters {
  amplitude: number;
  center: number;
  phaseShift?: number;
  phaseShiftDegress: number;
  frequency?: number;
  periodInSeconds?: number;
}
```

A sinusoid signal has the following parameters

* `amplitude` is the amplitude of sinusoid
* `center` is the center of the sinusoid
* phase shift is expressed as one of
    - `phaseShift` is the phase shift in radians.
    - `phaseShiftDegrees` can be used instead to express phase shift in degrees
    - If neither is specified, `phaseShift` defaults to 0 radians
* frequency is expressed as one of
    - `frequency` is the frequency of the sinsoid in Hz.
    - `periodInSeconds` can be used to express frequency as the period in seconds.
    - If neither is specified, default is `frequency` = 1/60 Hz (one cycle in 60 seconds).

![Sinusoid signal](sinusoid-plot.jpeg)
