## Get root
Get root of asset hierarchy.

### Signature
```
illumass.asset.getRoot()
```

### Access control
User must be signed in to access.

### Arguments

* _assetKey_ of asset.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt;.
