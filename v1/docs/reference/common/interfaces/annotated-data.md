## Annotated data
Data that is annotated with metadata. In the future there will be additional
properties to show the age or state of the data, especially in offline cases.

```
interface AnnotatedData<Data> {
  data: Data;
}
```

_data_ is the actual content that is being annotated.
