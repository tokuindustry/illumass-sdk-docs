## List
List measurement units

### Signature
```
illumass.measurementUnit.list()
```

### Access control
User must be signed in to access.

### Arguments

There are no arguments.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MeasurementUnit](../../../entities/measurement-units.md)&gt;&gt;.
