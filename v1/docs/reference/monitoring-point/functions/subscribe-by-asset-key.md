## Subscribe by asset key
Subscribe to all monitoring points of an asset.

### Signature
```
illumass.monitoringPoint.subscribeByAssetKey(assetKey: string, callback:(change: AnnotatedData<MonitoringPoint>) => void)
```

### Access control
User must be signed in to access.

### Arguments

* _assetKey_ is asset of monitoring points to subscribe to.
* _callback_ if function that is called every time a monitoring point is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MonitoringPoint](../../../entities/monitoring-points.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
