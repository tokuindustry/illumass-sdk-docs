## Subscribe
Subscribe to alarms for given signal.

### Signature
```
illumass.alarm.subscribeBySignalKey(signalKey: string, callback:(change: AnnotatedData<Alarm>) => void)
```

### Access control
User must be signed in to access.

### Arguments

* _signalKey_ of signal.
* _callback_ if function that is called every time alarm is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Alarm](../../../entities/alarms.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
