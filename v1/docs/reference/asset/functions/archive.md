## Archive
Archive an asset

### Signature
```
illumass.asset.archive(assetKey: string)
```

### Access control
User must be signed in and must have admin permission on the user.

### Arguments

* _assetKey_ is the asset to be archived. It should have the
    form `/assets/<asset id>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt;.
It contains the asset before it was archived.

Throws a 404 Not Found error if asset with _assetKey_ does not exist.
