## Update
Update comment

### Signature
```
illumass.comment.update(change: CommentChange)
```

### Access control
User must be signed in and must have write permission on the linked entity.

### Arguments

* _change_ is the [comment](../../../entities/comments.md) to be updated. `key` must be specified and it should
    have the form `/comments/<comment id>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Comment](../../../entities/comments.md)&gt;.
It contains the updated comment.

Throws a 404 Not Found error if comment with _change.key_ does not exist.
