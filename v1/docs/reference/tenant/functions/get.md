## Get
Get tenant

### Signature
```
illumass.tenant.get(tenantKey: string)
```

### Access control
User must be signed in to access and have View permission on _tenantKey_.

### Arguments

* _tenantKey_ of tenant.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Tenant](../../../entities/tenants.md)&gt;.

Throws a 403 Forbidden if user does not have View permission on _tenantKey_.
Throws a 404 Not Found error if tenant with _tenantKey_ does not exist.
