## Get
Get user

### Signature
```
illumass.user.get(userKey: string)
```

### Access control
User must be signed in to access and have View permission on _userKey_.

### Arguments

* _tenantKey_ of tenant.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[User](../../../entities/users.md)&gt;.

Throws a 403 Forbidden if user does not have View permission on _userKey_.
Throws a 404 Not Found error if user with _userKey_ does not exist.
