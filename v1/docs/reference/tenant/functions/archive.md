## Archive
Archive a tenant

### Signature
```
illumass.tenant.archive(tenantKey: string)
```

### Access control
User must be signed in and must have admin permission on the tenant.

### Arguments

* _tenantKey_ is the tenant to be archived. It should have the
    form `/tenants/<tenant id>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Tenant](../../../entities/tenants.md)&gt;.
It contains the tenant before it was archived.

Throws a 404 Not Found error if tenant with _tenantKey_ does not exist.
