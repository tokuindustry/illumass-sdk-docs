# Load a fixture

Allows developers and testers to load a tenant for the purposes of testing.

## Signature

`illumass.testing.loadFixture(fixture: string, json?: boolean)`

## Access control

User needs to be granted system admin permission.

## Arguments

* `fixture` is the yaml or json description of the fixture. The JSON schema can
  be found [here](load-fixture-schema.json).
* `json` is an optional boolean. Set to true if `fixture` is in json format.
  Otherwise `fixture` is assumed to be in yaml format.

## Resolves

Resolve to a dictionary of string replacements for all fixture variables.

## Fixture Variables

A fixture variable can be expressed as `${<varName>}`. The api call will first
replace them with a random alphanumeric string.

Length By default the length of the string is 16. To specify the length, append
after the variable name `:<length>`. For example, if we want the firstName to be
8 characters, use `${firstName:8}`.

Uppercase/Lowercase By default the case of the string is mixed. To force
uppercase, append after the variable name `:upper`. To force lowercase, append
`:lower` after the variable name.

## Examples

### Example 1

* [Fixture](fixture-example-1.yaml)
* [Dictionary](dictionary-example-1.json)

### Example 2

* [Fixture](fixture-example-2.yaml)
* [Dictionary](dictionary-example-2.json)
