## Subscribe
Subscribe to a single device by key

### Signature
```
illumass.device.subscribe(deviceKey: string, callback:(change: AnnotatedData<Device>) => void)
```

### Access control
User must be signed in to access and have View permission on _deviceKey_.

### Arguments

* _deviceKey_ of device.
* _callback_ if function that is called every time device is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Device](../../../entities/devices.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
