## Linear signal parameters
Parameters for a linear signal.

```
interface LinearSignalParameters {
  origin: moment.Moment;
  b?: number;
  m: number;
}
```

A linear signal has the following parameters

* `origin` is where the signal crosses `b`.
* `m` is the slope.
* `b` is an optional offset that is added. It specified where origin crosses `y = b`. By default `b = 0`.

![Linear signal](linear-plot.jpeg)
