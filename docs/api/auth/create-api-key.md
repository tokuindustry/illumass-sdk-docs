# Create API key

Create an API key.

## Examples

=== "Typescript"

    ```typescript
    const apiKey = await illumass.auth.createApiKey(description, options);
    ```

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl -H 'Authorization: Bearer <jwt>' \
    -H 'Content-Type: application/json' \
    --data '{ "description": "<description>" }' \
    'https://api.illumass.com/v4/api-keys/_create[?expiresIn=1h]'
    ```

    **WebSocket**

    ```json
    {
      "controller": "auth",
      "action": "createApiKey",
      "body": {
        "description": "<description>"
      },

      // optional arguments
      "_id": "api-key-id",
      "expiresIn": "1h",
    }
    ```

    **Response**

    ```json
    {
      "requestId": "ba0c7537-3c61-45f0-a798-de844284570e",
      "status": 200,
      "error": null,
      "controller": "auth",
      "action": "createApiKey",
      "collection": null,
      "index": null,
      "volatile": null,
      "result": {
        "_id": "oOJ9CHQB7KmymBNZ3uuL",
        "_source": {
          "description": "<description>",
          "expiresAt": 1597873900806,
          "fingerprint": "<jwt fingerprint>",
          "token": "<jwt>",
          "ttl": 3600000,
          "userId": "0005000000000008",
          "_kuzzle_info": {
            "author": null,
            "createdAt": 1597870300809,
            "updatedAt": null,
            "updater": null
          }
        }
      }
    }
    ```

## Parameters

- *description*
    Required. Purpose of API key.

- *expiresIn*
    Optional. Defaults to 1 hour. Time-to-live for JWT, expressed as a format that
    can be parsed by the [ms library](https://www.npmjs.com/package/ms).

## Response

- `result`
    - `_id` API key id
    - `_source`
        - `description`
        - `expiresAt` expiration date in milliseconds since Epoch
        - `ttl` time to live in milliseconds
        - `fingerprint` SHA256 hash of the authentication token
        - `token` JWT to be used in requests for this API key.

!!! important
    The response is the only time the API key's `token` will be returned by the server.
