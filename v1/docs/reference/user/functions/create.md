## Create
Create user

### Signature
```
illumass.user.create(change: UserChange)
```

### Access control
User must be signed in and be an administrator of the tenant.

### Arguments

* _change_ is the [user](../../../entities/users.md) to create. If `key` is not specified,
    it will be auto-generated. If it is specified, it should have the form
    `/users/<client id>`. The client id should be a value that has a very
    high probability of being unique. For example
    [uuid](https://www.npmjs.com/package/uuid) v4 should be adequate, as well as
    more compact forms of ids.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[User](../../../entities/users.md)&gt;.
