## Get
Get asset

### Signature
```
illumass.asset.get(assetKey: string)
```

### Access control
User must be signed in to access and have View permission on _assetKey_.

### Arguments

* _assetKey_ of asset.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt;.

Throws a 403 Forbidden if user does not have View permission on _assetKey_.
Throws a 404 Not Found error if asset with _assetKey_ does not exist.
