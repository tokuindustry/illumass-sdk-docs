# Signal Types

A signal type indicates the type of measurement being observed.

```
{
  "lastAudit": {
    "userKey": ""
  },
  "key": "/signalTypes/processStaticPressure",
  "name": "ProcessStaticPressure",
  "displayName": "Pressure",
  "measurementType": "pressure",
  "ancillary": false
}
```

## Properties

* _name_ is the internal name used by the system.
* _displayName_ is a text string suitable for display purposes.
* _measurementType_ indicates the type of measurement. This is used to
  determine which [measurement units](./measurement-units.md) are applicable.
* _ancillary_ is true if this is measuring some aspect of the sensor or device,
  and **not** some aspect of an asset.
