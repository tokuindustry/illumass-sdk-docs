# Downscale slice

Downscale a slice

## Signature

```typescript
illumass.signal.downscaleSlice(
  signal: SignalDescriptor,
  interval: IntervalDescriptor,
  sampleDurationInSeconds: number
)
```

## Access control

User needs to be granted view permission on the device or monitoring point of
the signal.

## Arguments

* *signal* is a [SignalDescriptor](../interfaces/signal-descriptor.md) that
describes the signal to fetch slice from.
* *interval* is a [IntervalDescriptor](../interfaces/interval-descriptor.md)
that describes the interval fetch slice from.
* *sampleDurationInSeconds* is the amount of time each sample represents. For
example, if you had a signal with 1 second resolution, a *sampleDurationInSeconds*
of 60 seconds would create a sample for every minute. Each sample would would
consists of 60 values.

## Resolves

Resolves to a [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[SampledSlice](../interfaces/sampled-slice.md)&gt;.
