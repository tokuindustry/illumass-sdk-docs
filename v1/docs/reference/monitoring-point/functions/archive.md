## Archive
Archive a monitoring point

### Signature
```
illumass.monitoringPoint.archive(monitoringPointKey: string)
```

### Access control
User must be signed in and must have edit permission on the asset.

### Arguments

* _monitoringPointKey_ is the monitoring point to be archived. It should have the
    form `/monitoringPoint/<monitoring point id>`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[MonitoringPoint](../../../entities/monitoring-points.md)&gt;.
It contains the monitoring point before it was archived.

Throws a 404 Not Found error if monitoring point with _monitoringPointKey_ does not exist.
