# Entities

## Schema

Entities are described using [JSON schema](https://json-schema.org/). The schema
can be found [here](entities/schema.json).

## Keys

Entity keys uniquely identify an entity in Illumass. They should be treated as 
opaque strings whenever possible.

An entity key has the format `/<entity type>/<globally unique id>`.

* `<entity type>` is one of
    * `alarms`
    * `assets`
    * `assetTypes`
    * `assetRoles`
    * `comments`
    * `countries`
    * `devices`
    * `measurementUnits`
    * `monitoringPoints`
    * `signals`
    * `signalTypes`
    * `schemas`
    * `tenants`
    * `users`

* `<globally unique id>` is a string that matches the regex `/^[a-zA-Z0-9][-_a-zA-Z0-9]{16:64}$/`.
    * `signalTypes`, `countries`, and `measurementUnits` do not have minimum 
    length restrictions.

## Common Properties

There are common properties that can be found on many entities.

* _archived_ indicates if the entity has been archived.
* _key_ is the key used to identify the entity. See [Keys](#keys).
* _lastAudit_ contains the last user who made a change and when.
* _props_ contains custom properties. Custom properties are specific to application 
and customer.
* _tenantKey_ is the tenant the entity belongs to.

## Entity Types

* [Alarms](entities/alarms.md)
* [Assets](entities/assets.md)
* [Asset Types](entities/asset-types.md)
* [Asset Roles](entities/asset-roles.md)
* [Comments](entities/comments.md)
* [Countries](entities/countries.md)
* [Devices](entities/devices.md)
* [Measurement Units](entities/measurement-units.md)
* [Monitoring Points](entities/monitoring-points.md)
* [Signals](entities/signals.md)
* [Signal Types](entities/signal-types.md)
* [Schemas](entities/schemas.md)
* [Tenants](entities/tenants.md)
* [Users](entities/users.md)

An [entity-relationship diagram](./entities/images/illumass-er.pdf) illustrates
the relationships among the major entities.
