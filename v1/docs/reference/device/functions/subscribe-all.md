## Subscribe
Subscribe to all devices that user can view.

### Signature
```
illumass.device.subscribeAll(callback:(change: AnnotatedData<Device>) => void)
```

### Access control
User must be signed in to access.

### Arguments

* _callback_ if function that is called every time a device is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Device](../../../entities/devices.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
