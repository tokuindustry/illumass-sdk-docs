## Page
Page of results

```
interface Page<Item> {
  readonly items: ReadonlyArray<Item>;
  readonly nextPage: () => Promise<Page<Item>>;
  readonly hasNextPage: boolean;
}
```

### Properties
* _items_ on this page
* _hasNextPage_ indicates if there is another page after this one

### Methods
* _nextPage()_ fetches the next page
