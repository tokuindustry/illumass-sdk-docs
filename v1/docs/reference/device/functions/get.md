## Get
Get device

### Signature
```
illumass.device.get(deviceKey: string)
```

### Access control
User must be signed in to access and have View permission on _deviceKey_.

### Arguments

* _deviceKey_ of device. The device key has the form `/devices/${serialNumber}`.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Device](../../../entities/devices.md)&gt;.

Throws a 403 Forbidden if user does not have View permission on _deviceKey_.
Throws a 404 Not Found error if device with _deviceKey_ does not exist.
