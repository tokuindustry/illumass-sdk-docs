## Subscribe all
Subscribe to all assets that user can view in the current tenant.

### Signature
```
illumass.asset.subscribeAll(callback:(change: AnnotatedData<Asset>) => void)
```

### Access control
User must be signed in to access.

### Arguments

* _callback_ if function that is called every time an asset is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
