## List
List asset roles that user had view permission for.

### Signature
```
illumass.assetRole.list()
```

### Access control
User must be signed in to access.

### Arguments

There are no arguments.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[AssetRole](../../../entities/asset-roles.md)&gt;&gt;.
