# Overview

## Illumass Typescript SDK

The Illumass Typescript SDK can be found on [npm](https://www.npmjs.com/package/@illumass/illumass-sdk).

## Goals

SDK 4 makes the following improvements.

### Improve performance when reading signals

Reading signal time-series data is extremely slow. Server improvements have
been made to address this, but requires calling a new API endpoint.

### Subsecond sampling rates

SDK 4 adds support for signals with subsecond sampling.

### Introduce human-readable entity references

Although id's are adequate for machine to machine integrations, it is unwieldly
for people to use and think about.

An entity reference scheme based on the name of the entities is introduced to
more easily reference entities in code.

### Additional formats for signals

SDK 4 introduces a csv and JSON format. Furthermore, there are options
to control how timestamps are encoded. Environments like R and Python represent time
as the number of seconds since epoch.

### API key support

Current integration software requires the username/password to be stored in some
fashion. This is considred to be a poor security practice.

To address this, API key were introduced. Instead of storing username/password,
the API key is stored. They should be regularly revoked and replaced. In the
event an API key is known to be compromised, they can be manually revoked.

### Flatter representation of entities

The entity JSON has changed significantly. By flattening most of the fields
it should be easier to work with.

--8<-- "mermaid.txt"
