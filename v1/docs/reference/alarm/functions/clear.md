## Clear alarm
Clear the given alarm.

This is a convenience function that calls `illumass.alarm.update` with the
correct argument.

### Signature
```
illumass.alarm.clear(alarmKey: string)
```

### Access control
User must be signed in and must have write permission on the signal's holder.

### Arguments

* _alarmKey_ of alarm to be cleared.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Alarm](../../../entities/alarms.md)&gt;.
It contains the updated alarm.

Throws a 404 Not Found error if alarm with _alarmKey_ does not exist.
