## Constant signal parameters
Parameters for a constant signal.

```
interface ConstantSignalParameters {
  reading: number;
}
```

A constant signal has only one parameter (`reading`) which is the value to use
throughout the signal.

![Constant signal](constant-plot.jpeg)
