# ER diagram

```mermaid
erDiagram
	assets {
		int id
		string doc_id
		string name
	}
  hardpoints {
    int id
    string doc_id
    string name
  }
  devices {
    int id
    string doc_id
    string serial_number
  }
  signals {
    int id
    string doc_id
    string name
  }
  signal_data {
    int signal_id
    timestamptz t
    real y
  }
  assets ||--o{ hardpoints : have
  hardpoints |o--o{ signals : have
  devices |o--o{ signals : have
  signals ||--o{ signal_data : data
```

- for all tables, except for `signal_data`, `id` is a primary key and `doc_id`
is a candidate key.
- `assets` are things in the real world that are monitored. e.g. Pumpjack
- `hardpoints` are where devices can be installed on `assets`. It is usually a
monitoring point of interest. e.g. Tubing
- `signals` is synonymous with sensors. It represents a time-series.
- `signal_data` is the actual time-series data associated with a signal.
- `devices` is usually the actual hardware that is collecting data. Note that
it can have one or more sensors/signals.
    - `serial_number` is a candidate key.
- the primary key for `signal_data` is `(signal_id, t)`.

--8<-- "mermaid.txt"
