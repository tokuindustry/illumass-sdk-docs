# Asset Types

Asset types represents different types of assets that a tenant has.

```json
{
  "lastAudit": {
    "userKey": ""
  },
  "key": "/assetTypes/8009000000000004",
  "archived": false,
  "name": "Well"
}
```

## Properties

* _name_ is the name of the asset type. It should be unique within the tenant.

## Public asset types

The following asset types are available to all tenants and users.

1. Pipeline
1. Folder
1. Tank
1. Well
1. Riser
1. Satellite
1. Battery
1. Water Plant
1. Gas Plant
1. Field
1. District
1. Run
1. Area
