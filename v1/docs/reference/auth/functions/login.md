## Login
Login with email and password

### Signature
```
illumass.auth.login(
  email: string,
  password: string,
  expiresIn?: string
)
```

### Access control
Anonymous users can access.

### Arguments

* `email` is the user's email
* `password` is the user's password
* `expiresIn` is an optional string that expresses the expiration in [ms library](https://www.npmjs.com/package/ms) format. (e.g. `2h`)

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data/)&lt;string&gt;,
where the string is a [JSON Web Token](https://jwt.io).
