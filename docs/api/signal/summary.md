# Summary

Get the summary values for a signal. The summary includes the mean, maximum,
minimum, variance, and count for each group.

## Examples

=== "Typescript"

    ```typescript
    const timeSeries = illumass.signalData.summary(ref, start, end, resolution, options);
    ```

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl -H 'Authorization: Bearer <jwt>' \
    'https://api.illumass.com/v4/signal/summary/signal!<signal id>?start=2020-08-17T19:00:00Z&duration=PT5H&resolution=hour&format=json'
    ```

    **WebSocket**

    ```json
    {
      "action": "summary",
      "controller": "rumble/signal",
      "ref": "signal!<signal id>",
      "start": "2020-08-14T19:00:00Z",
      "duration": "PT5H",
      "resolution": "hour",
      "format": "json"
    }
    ```

    **Response**

    ```json
    {
      "summaries": {
        "timestamps": ["2020-08-17T19:00:00Z", "2020-08-17T20:00:00Z", "2020-08-17T21:00:00Z", "2020-08-17T22:00:00Z", "2020-08-17T23:00:00Z"],
        "means": [6.549316883087158, 6.524133205413818, 6.396183967590332, 6.385933876037598, 6.381115913391113],
        "maximums": [6.5879998207092285, 6.576000213623047, 6.40500020980835, 6.416999816894531, 6.453999996185303],
        "minimums": [6.38100004196167, 6.38100004196167, 6.331999778747559, 6.2829999923706055, 6.295000076293945],
        "variances": [0.003059739712625742, 0.004413972608745098, 0.00010889863915508613, 0.000277992628980428, 0.00027146877255290747],
        "counts": [60, 60, 60, 60, 60]
      }
    }
    ```

## Parameters

- *ref*

    Required. [Signal entity reference](../../../entities/references#signals).

- *start*, *end*, *duration*

    Required. The interval of the signal data to retrieve. At least one of *start*
    or *end* must be specified.

    - *start* [ISO 8601 date time](https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations)
    for start of interval, inclusive.
    - *end* [ISO 8601 date time](https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations)
    for end of interval, exclusive.
    - *duration* [ISO 8601 duration](https://en.wikipedia.org/wiki/ISO_8601#Durations).
    Duration is consided only if one of *start* or *end* is missing. Default to
    1 hour.

- *resolution*

    Required. Resolution of signal data to get. Must be one of *hour*, *minute*,
    or *second*. Futhermore, it should be a larger time scale than the devices
    default resolution. For example, if a signal has a 1 second resolution,
    then a resolution of *minute* or *hour* should be used.

- *measuringUnit*

    Optional. Can be one of *source*, *tenant*, or a *measuring-unit* entity reference.

    - If *source*, then uses the signal default.
    - If *tenant*, then converts to the measuring unit configured for the tenant.
    - If a *measuring-unit* entity reference, then converts to the specified
    measuring unit.

    Defaults to *source*.

- *format*

    Optional. Format the signal data.

    - *binary*. Data is encoded as protobuf binary.
    - *base64*. Data is encoded as base64, protobuf binary.
    - *json*. Data is encoded as two json arrays. One array is the timestamps,
    the other is the signal values.

        ```json
        {
          "raws": {
            "timestamps": […],
            "values": […],
          }
        }
        ```

    - *csv*. Data is encoded as a csv with two colums.
    The first column is the timestamp, the second is the signal value.

    Defaults to *base64*.

- *datetime*

    Optional. When *format* is *json* or *csv*, this specifies the format to use
    for timestamps. The valid values are

    - *iso*, *iso8601*. Timestamps are formatted as [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).
    - *excel*, *csv*. Use Excel friendly timestamp format YYYY-MM-DD HH:mm:ss.
    - *milliseconds*, *millisecond*, *ms*. Milliseconds since 1970-01-01T00:00Z.
    Useful for programming languages that represent time as millseconds.
    e.g. JavaScript, Java
    - *seconds*, *second*, *s*. Seconds since 1970-01-01T00:00Z.
    Useful for programming languages that represent time as seconds.
    e.g. Python, R

    Defaults to *iso8601*

- *timezone*

    Optional. When *format* is *json* or *csv*, this specifies the [tz database name](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
    to use for timestamps. Defaults to *UTC*.
