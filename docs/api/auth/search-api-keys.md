# Search API keys

Search user's API keys.

## Examples

=== "Typescript"

    ```typescript
    const apiKeys = await illumass.auth.searchApiKeys();
    ```

=== "HTTP/WebSocket"

    **cURL**

    ```bash
    curl -H 'Authorization: Bearer <jwt>' \
    -H 'Content-Type: application/json' \
    --data '{}' \
    'https://api.illumass.com/v4/api-keys/_search[?from=0][&size=100]'
    ```

    **WebSocket**

    ```json
    {
      "controller": "auth",
      "action": "searchApiKeys",
      "body": {},

      // optional arguments
      "from": 0,
      "size": 10
    }
    ```

    **Response**

    ```json
    {
      "requestId": "f8f08496-c0e5-4eec-b2bc-1b4a6732f730",
      "status": 200,
      "error": null,
      "controller": "auth",
      "action": "searchApiKeys",
      "collection": null,
      "index": null,
      "volatile": null,
      "result": {
        "hits": [{
          "_id": "DZRAl3MBddfEOpjR_qD97afq57a7",
          "_source": {
            "description": "NodeRED",
            "expiresAt": 1598562484153,
            "fingerprint": "620c3da82900e13f5934d671856f034c9ac3481e9baba69cf4fefd42b3de9646",
            "ttl": 2592000000,
            "userId": "0005000000000008"
          }
        }, {
          "_id": "oOJ9CHfdwttf134QB7KmymBNZ3ufeuL",
          "_source": {
            "description": "Salesforce integration",
            "expiresAt": 1597873900806,
            "fingerprint": "8df95a8fafc8ceecd27fb0ee2f472319853c9316fae163990403c1ba80321e70",
            "ttl": 3600000,
            "userId": "0005000000000008"
          }
        }],
        "total": 2
      }
    }
    ```

## Parameters

- *body*
    Required. Body is the search query. An empty object `{}` matches all user's
    API keys.
