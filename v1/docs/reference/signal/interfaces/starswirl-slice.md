# Slice collection

A slice.

```typescript
interface ISlice {

    /** Slice start */
    start?: (google.protobuf.ITimestamp|null);

    /** Slice period */
    period?: (google.protobuf.IDuration|null);

    /** Slice values */
    values?: (number[]|null);
}
```

## Properties

* `start` is the start time of the slice
* `period` is the period between two subsequent values
* `values` are the values in the slice
