## Update
Update alarm

### Signature
```
illumass.alarm.update(change: AlarmChange)
```

### Access control
User must be signed in and must have write permission on the signal's holder.

### Arguments

* _change_ is the [alarm](../../../entities/alarms.md) to be updated. `key` must be specified and it should
    have the form `/alarms/<alarm id>`.

    The only field that is usually set is the _cleared_ field. It should be a
    UTC timestamp in ISO 8601.

    The other field that can be set is the _clearerKey_. This is a user key and
    is usually the key of the logged in user. This can be set to another user,
    so long as the currently logged in user has admin permission on the another
    user.

### Resolves
Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Alarm](../../../entities/alarms.md)&gt;.
It contains the updated alarm.

Throws a 404 Not Found error if alarm with _change.key_ does not exist.
