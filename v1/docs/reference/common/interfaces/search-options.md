## Search Options

Search options

```typescript
{
  from?: number;
  size?: number;
  scrollId?: string;
  includeTrash?: boolean;
  volatile?: {
    illumass?: {
      tenantKey?: string;
    },
  };  
}
```

## Properties

* _from_ is the offset of first document to fetch
* _size_ is maximum number of documents to retrieve per page
* _scroll_ When set, gets a forward-only cursor having its ttl set to the given value. (i.e. "30s")
* _includeTrash_ to include deleted items in search. This in general should not be used.
* _volatile.illumass.tenantKey_ indicates the tenant to perform search on.
    * if it is not specified or *undefined*, the devices for the current tenant
    that the user can view will be listed.
    * if a tenant key is specified (e.g. */tenants/<tenant id>*), entities for
    the given tenant that the user can view will be listed.
    * if **&ast;** is specified, any entity in the system that the user can view
    will be listed.
