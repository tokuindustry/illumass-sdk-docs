# Kuzzle Admin

## Setup

1. Open browser and go to [Kuzzle Admin Tool](https://dev.tokuindustry.com:7513)
1. Initially you will be asked to create a connection to Kuzzle.
    ![Create Connection](kuzzle-admin-create-connection.png)
    * Choose a **Name** that will help you remembe what the connection is for.
    * **Host** should be `dev.tokuindustry.com/v3/`. Note that the scheme prefix (e.g. http://) is removed.
    * **Port** should be `443`
    * **use SSL** should be enabled. Note that there is a bug that does not let you simply click the checkbox. You need to hit the *Tab* key until the checkbox is focussed. Once it is focussed, hit the *space* key to enble.
    * Choose a color and click on *Create Connection* at the bottom.


1. The login screen will now appear.
    * Login is *admin*. Password is *masterkey*.


1. You should now see the main admin console.
