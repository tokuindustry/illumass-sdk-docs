## List comments by author key
List all comments made by a user.

### Signature
```
illumass.comment.listByAuthorKey(authorKey: string)
```

### Access control
User must be signed in to access.

### Arguments

* _authorKey_ is user to list comments for. It should have the form `/users/<user id>`.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Comment](../../../entities/comments.md)&gt;&gt;.
