# Common queries

## T1000 signals

Available signals on T1000 are

| Name | Sampling period | Regular | Unit |
|:---|:---|:---:|:---:|
| `Ambient temperature` | 1 minute | Yes | Celsius |
| `Battery charge available` | 1 minute | Yes | Ah |
| `Battery current` | 1 minute | Yes | A |
| `Battery voltage` | 1 minute | Yes | V |
| `Device uptime` | 1 hour | No | Seconds |
| `Network bit error rate` | 1 hour | No | Scalar |
| `Network data time` | 1 hour | No | Seconds |
| `Network power on time` | 1 hour | No | Seconds |
| `Network registration time` | 1 hour | No | Seconds |
| `Network status code` | 1 hour | No | Scalar |
| `Pressure` | 1 second | Yes | kPa |
| `Pressure sensor drive voltage` | 1 minute | Yes | V |
| `Pressure sensor output voltage` | 1 minute | Yes | V |
| `Pressure sensor temperature voltage` | 1 minute | Yes | V |
| `Sensor temperature` | 1 minute | Yes | Celsius |
| `Signal strength` | 1 hour | No | dBm |
| `Solar charge voltage` | 1 minute | Yes | V |

## Find signal id by asset name, hardpoint name, and signal name

=== "PostgreSQL"

    ```postgresql
    SELECT s.id
    FROM assets a
      JOIN hardpoints h on a.id = h.asset_id
      JOIN signals s on h.id = s.hardpoint_id
    WHERE a.name = $1
      AND h.name = $2
      AND s.name = $3
    ```

=== "SQL Server"

    ```tsql
    SELECT s.id
    FROM assets a
      JOIN hardpoints h on a.id = h.asset_id
      JOIN signals s on h.id = s.hardpoint_id
    WHERE a.name = $1
      AND h.name = $2
      AND s.name = $3
    ```

- `$1` is name of asset
- `$2` is name of hardpoint
- `$3` is name of signal. See [available signals](#t1000-signals).

## Find signal id by device serial number and signal name

=== "PostgreSQL"

    ```postgresql
    SELECT s.id
    FROM devices d
      JOIN signals s ON d.id = s.device_id
    WHERE d.serial_number = $1
      AND s.name = $2
    ```

=== "SQL Server"

    ```tsql
    SELECT s.id
    FROM devices d
      JOIN signals s ON d.id = s.device_id
    WHERE d.serial_number = $1
      AND s.name = $2
    ```

- `$1` is device serial number
- `$2` is signal name

## Get latest value for a signal

=== "PostgreSQL"

    ```postgresql
    SELECT sd.t, sd.y
    FROM signal_data sd
    WHERE sd.signal_id = $1
    ORDER BY sd.t DESC
    LIMIT 1
    ```

=== "SQL Server"

    ```tsql
    SELECT TOP 1 sd.t, sd.y
    FROM signal_data sd
    WHERE sd.signal_id = $1
    ORDER BY sd.t DESC
    ```

- `$1` is the signal id

## Signal data for a time range

=== "PostgreSQL"

    ```postgresql
    SELECT t, y
    FROM signal_data
    WHERE signal_id = $1
        AND t BETWEEN $2 AND $3
    ORDER BY t
    ```

=== "SQL Server"

    ```tsql
    SELECT t, y
    FROM signal_data
    WHERE signal_id = $1
        AND t BETWEEN $2 AND $3
    ORDER BY t
    ```

- `$1` is the signal id
- `$2` and `$3` are the *from* and *to* timestamps, respectively

## Signal rate of change for a time range

=== "PostgreSQL"

    ```postgresql
    SELECT s.t,
        CAST((lead(s.y) OVER w1 - lag(s.y) OVER w1) / EXTRACT( epoch FROM lead(s.t) OVER w1 - lag(s.t) OVER w1) AS REAL) AS y
      FROM signal_data s
      WHERE s.signal_id = $1 AND s.t BETWEEN $2 AND $3
      WINDOW w1 AS (ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING EXCLUDE CURRENT ROW)
      ORDER BY s.t
    ```

=== "SQL Server"

    `TODO`

- `$1` is the signal id
- `$2` and `$3` are the *from* and *to* timestamps, respectively

## List of all signals with latest readings

=== "PostgreSQL"

    ```postgresql
    SELECT a.name, h.name, s.name, sd.t, sd.y
    FROM assets a
    JOIN hardpoints h ON a.id = h.asset_id
    JOIN signals s ON h.id = s.hardpoint_id
    JOIN LATERAL (
        SELECT x.t, x.y
        FROM signal_data x
        WHERE x.signal_id = s.id
        ORDER BY x.t DESC
        LIMIT 1
    ) sd ON true
    ```

=== "SQL Server"

    `TODO`

## Latest reading by asset name, hardpoint name, and signal name

=== "PostgreSQL"

    ```postgresql
    SELECT sd.t, sd.y
    FROM signal_data sd
    WHERE sd.signal_id = (
      SELECT s.id
      FROM assets a
        JOIN hardpoints h on a.id = h.asset_id
        JOIN signals s on h.id = s.hardpoint_id
      WHERE a.name = $1
        AND h.name = $2
        AND s.name = $3
    )
    ORDER BY sd.t DESC
    LIMIT 1
    ```

=== "SQL Server"

    ```tsql
    SELECT TOP 1 sd.t, sd.y
    FROM signal_data sd
    WHERE sd.signal_id = (
      SELECT s.id
      FROM assets a
        JOIN hardpoints h on a.id = h.asset_id
        JOIN signals s on h.id = s.hardpoint_id
      WHERE a.name = $1
        AND h.name = $2
        AND s.name = $3
    )
    ORDER BY sd.t DESC
    ```

- `$1` is name of asset
- `$2` is name of hardpoint
- `$3` is name of signal. See [available signals](#t1000-signals).
