# Current login context

Get the current login context. The current login context can be modified with [setLoginContextTenantKey](./set-login-context-tenant-key.md).

## Signature

```typescript
illumass.auth.currentLoginContext()
```

## Access control

Signed in users can call this.

## Arguments

There are no arguments

## Resolves

Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[ActionContext](../interfaces/action-context.md)&gt;.
