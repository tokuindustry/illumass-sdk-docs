# Default login context

Get the default login context for the currently signed in user.

## Signature

```typescript
illumass.auth.defaultLoginContext()
```

## Access control

Signed in users can call this.

## Arguments

There are no arguments

## Resolves

Resolves to [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[ActionContext](../interfaces/action-context.md)&gt;.
