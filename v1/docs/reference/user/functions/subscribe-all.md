## Subscribe all
Subscribe to all users that user can view in the current tenant.

### Signature
```
illumass.user.subscribeAll(callback:(change: AnnotatedData<User>) => void)
```

### Access control
User must be signed in to access.

### Arguments

* _callback_ if function that is called every time a user is changed.
    The callback will receive an [AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[User](../../../entities/users.md)&gt; object.

### Resolves
Resolves to [Subscription](../../common/interfaces/subscription.md).
