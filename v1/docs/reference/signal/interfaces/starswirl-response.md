# Response

Response from a [transform](../functions/transform.md) request.

```typescript
interface IResponse {

    /** Response status */
    status?: (number|null);

    /** Response message */
    message?: (string|null);

    /** Response outputSlices */
    outputSlices?: ({ [k: string]: toku.starswirl.protobuf.ISliceCollection }|null);
}
```

## Properties

* `status` is a status code that indicates the status of the transform operation.
It uses the same values as HTTP status codes.
* `message` is an optional text string that contains additional information about
the transform operation. This is usually an error message.
* `outputSlices` is an object where the keys are the output names of the transform
call and the value is a [slice collection](./starswirl-slice-collection.md).
