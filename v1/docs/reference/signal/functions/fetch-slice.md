# Fetch slice

Fetch a slice of a signal

## Signature

```typescript
illumass.signal.fetchSlice(
  signal: SignalDescriptor,
  interval: IntervalDescriptor
)
```

## Access control

User needs to be granted view permission on the device or monitoring point of
the signal.

## Arguments

* `signal` is a [SignalDescriptor](../interfaces/signal-descriptor.md) that
describes the signal to fetch slice from.
* `interval` is a [IntervalDescriptor](../interfaces/interval-descriptor.md)
that describes the interval fetch slice from.

## Resolves

Resolves to a [AnnotatedData](../../common/interfaces/annotated-data.md)
&lt;[Slice](../interfaces/slice.md)&gt;. If there is no data at the given
interval, then the slice will be the empty object `{}`.
