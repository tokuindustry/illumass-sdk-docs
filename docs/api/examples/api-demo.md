# API Demo

There is a demo account called "API Demo" that you can use to try out the API.

The following shows how you can subscribe to a WebSocket to be notified of changes.

The Typescript examples are available on [github.com](https://github.com/TOKU-Systems/illumass-sdk-examples).
