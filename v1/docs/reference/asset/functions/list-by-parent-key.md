## List by parent key
List children assets.

### Signature
```
illumass.asset.listByParentKey(parentKey: string)
```

### Access control
User must be signed in to access and have browse permission on parentKey.

### Arguments

* _parentKey_ is the parent asset to list children for. Is should be have the
  form `/assets/<asset id>`.

### Resolves
Resolves to [Page](../../common/interfaces/page.md)&lt;[AnnotatedData](../../common/interfaces/annotated-data.md)&lt;[Asset](../../../entities/assets.md)&gt;&gt;.
