# Users

Users of Illumass

```json
{
  "lastAudit": {
    "userKey": "",
    "timestamp": "2019-05-14T17:52:06.600Z"
  },
  "key": "/users/2dE5u5Tnr5KNHvd2",
  "archived": false,
  "email": "2dE5u5Tnr5KNHvd2@test.com",
  "firstName": "AjvFBfwf",
  "lastName": "ZYaQDwFD",
  "alertBySms": false,
  "alertByEmail": false,
  "userType": "administrator",
  "props": {},
  "phoneNumber": null,
  "phoneNumberE164": null
}
```

## Properties

* User's _email_ address.
* User's _firstName_.
* User's _lastName_.
* _alertBySms_ indicates if user want's to receive notifications by SMS.
* _alertByEmail_ indicates if the user wants to receive notifications by email.
* _userType_ must be one of the available [user roles](../guides/concepts/security-roles.md#user-roles).
* _phoneNumber_ is the number that will receive the SMS texts.
* _phoneNumberE164_ is a read-only property that is a normalized version of _phoneNumber_.
